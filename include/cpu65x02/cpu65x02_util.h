/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
#ifndef CPU65X02_UTIL_H_
#define CPU65X02_UTIL_H_

#include "cpu65x02_types.h"
#include "cpu65x02_constants.h"

/**
 * @brief Increments tick counter by one
 *
 * @param cpu
 */
static inline void cpu65x02_tick(struct cpu65x02* cpu) {
    cpu->cycles += 1;
}

/**
 * @brief Increments tick counter by N
 *
 * @param cpu
 * @param n
 */
static inline void cpu65x02_tick_n(struct cpu65x02* cpu, uint8_t n) {
    cpu->cycles += n;
}

/**
 * @brief Builds a 16 bit word out of given low and high bytes
 *
 * @param low
 * @param high
 *
 * @return
 */
static inline uint16_t cpu65x02_word(uint8_t low, uint8_t high) {
    const uint16_t result = high;
    return (result << 8) | low;
}

/**
 * @brief Get next byte from the pipeline
 *
 * @param cpu
 *
 * @return
 */
static inline uint8_t cpu65x02_get_byte(struct cpu65x02* cpu) {
    return cpu->l(cpu->regs.pc++, cpu->user_data);
}

/**
 * @brief Push given value to stack
 *
 * @param cpu
 * @param data
 */
static inline void cpu65x02_stack_push(struct cpu65x02* cpu, uint8_t data) {
    cpu->s(CPU65X02_STACK_BASE + cpu->regs.s--, data, cpu->user_data);
}

/**
 * @brief Pop value from stack
 *
 * @param cpu
 *
 * @return
 */
static inline uint8_t cpu65x02_stack_pop(struct cpu65x02* cpu) {
    return cpu->l(CPU65X02_STACK_BASE + ++cpu->regs.s, cpu->user_data);
}

/**
 * @brief Push a 16 bit word to stack
 *
 * @param cpu
 * @param word
 */
static inline void cpu65x02_stack_push_word(struct cpu65x02* cpu, uint16_t word) {
    cpu65x02_stack_push(cpu, (word >> 8));
    cpu65x02_stack_push(cpu, (word & 0xff));
}

/**
 * @brief Pop a 16 bit word from stack
 *
 * @param cpu
 *
 * @return
 */
static inline uint16_t cpu65x02_stack_pop_word(struct cpu65x02* cpu) {
    const uint8_t low = cpu65x02_stack_pop(cpu);
    const uint8_t high = cpu65x02_stack_pop(cpu);
    return cpu65x02_word(low, high);
}

/**
 * @brief Push program counter to stack
 *
 * @param cpu
 */
static inline void cpu65x02_stack_push_pc(struct cpu65x02* cpu) {
    cpu65x02_stack_push_word(cpu, cpu->regs.pc);
}

/**
 * @brief Restore program counter from stack
 *
 * @param cpu
 */
static inline void cpu65x02_stack_pop_pc(struct cpu65x02* cpu) {
    cpu->regs.pc = cpu65x02_stack_pop_word(cpu);
}

/**
 * @brief Low program counter with an interrupt address stored under provided
 * vector
 *
 * @param cpu
 * @param vector_low
 */
static inline void cpu65x02_load_vector(struct cpu65x02* cpu,
        uint16_t vector_low) {
	cpu->regs.pc = cpu65x02_word(
            cpu->l(vector_low, cpu->user_data),
            cpu->l(vector_low + 1, cpu->user_data));
}

/**
 * @brief Add extra clock cycle on page boundary
 *
 * @param cpu
 * @param ba base address
 * @param a effective address
 */
static inline void cpu6502_pb_correct_cycles(struct cpu65x02* cpu,
        uint16_t ba, uint16_t a) {
    // add a cycle on page boundary crossed
    if ((ba & 0xff00) != (a & 0xff00)) {
        cpu65x02_tick(cpu);
    }
}

#endif /* CPU65X02_UTIL_H_ */
