#ifndef CPU65X02_DEBUG_H_
#define CPU65X02_DEBUG_H_

#include "cpu65x02_types.h"

#ifndef NDEBUG

/**
 * @brief Dumps the execution context
 *
 * @param cpu Pointer to the CPU instance
 */
void cpu65x02_dump_core(struct cpu65x02* cpu);

/**
 * @brief Disassemble single instruction
 *
 * This function prints the single instruction along with some context and
 * processor state.
 *
 * @param cpu CPU instance
 * @param address operand's address
 */
void cpu65x02_disasm_opcode(struct cpu65x02* cpu, uint16_t address);

/**
 * @brief Used for debugging to indicate a cpu fatal error
 *
 * @param cpu CPU instance
 */
void cpu65x02_fatal(struct cpu65x02* cpu);

#else
/* stubs to be optimized away */

static inline void cpu65x02_dump_core(struct cpu65x02* cpu) {}
static inline void cpu65x02_disasm_opcode(struct cpu65x02* cpu,
        uint16_t address) {}
static inline void cpu65x02_fatal(struct cpu65x02* cpu) {}

#endif /* NDEBUG */

#endif /* CPU65X02_DEBUG_H_ */
