/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
#ifndef CPU65X02_TYPES_H_B34KHYGL
#define CPU65X02_TYPES_H_B34KHYGL

#include <stdint.h>

/**
 * @brief Declares a type for memory loading routine
 *
 * This typedef defines a type for a function loading data from the memory for
 * the CPU. It takes a 16 bit address and a pointer to optional user data and
 * is expected to return a corresponding data under provided address.
 *
 * @param address
 * @param user_data user data (NULL if unused)
 *
 * @return data
 */
typedef uint8_t (*cpu65x02_load_byte_t)(uint16_t, void* user_data);

/**
 * @brief Declares a type for memory store routine
 *
 * This typedef defines a type for a function storing data in memory. It takes
 * a 16 bit address, the data itself and optional user data as an arguments.
 *
 * @param address
 * @param uint8_t data
 * @param user_data user data(NULL if unused)
 */
typedef void (*cpu65x02_store_byte_t)(uint16_t, uint8_t, void* user_data);

/**
 * @brief 65x02 registers
 *
 * Aggregates the registers of 6502/65C02 CPU
 */
struct cpu65x02_regs {
	/// accumulator
	uint8_t a;

	/// X index register
	uint8_t x;

	/// Y index register
    uint8_t y;

    /**
     * status register
     * @verbatim
     * Bit No.   7 6 5 4 3 2 1 0
     *           S V   B D I Z C
     * @endverbatim
     * - S - Sign bit
     * - V - Overflow bit
     * - B - Break (indicates if executing an IRQ or a BRK)
     * - D - Decimal mode
     * - I - Disable interrupts
     * - Z - Zero
     * - C - Carry
     */
	uint8_t status;

	/// stack pointer
	uint8_t s;

	/// program counter
    uint16_t pc;
};


struct cpu65x02;

/**
 * @brief Defines a type for a function calculating the address of the operand
 *
 * @param CPU instance
 *
 * @return calculated address
 */
typedef uint16_t (*cpu65x02_op_addresser_t)(struct cpu65x02* cpu);

/**
 * @brief Defines a type for a function implementing an op-code
 *
 * @param CPU instance
 * @param address operand's address (ignored if op-code doesn't accept arguments)
 */
typedef void (*cpu65x02_op_executor_t)(struct cpu65x02* cpu, uint16_t address);

/**
 * @brief Op-code handler definition
 *
 * Defines an op-code handler function along with the function
 * generating an address for the operand.
 */
struct cpu65x02_op_handler {
    /// Pointer to the routine generating operand's address
    cpu65x02_op_addresser_t addresser;

    /// Pointer to the routine which implements the op-code
    cpu65x02_op_executor_t executor;

    /// Base number of cycles
    uint8_t base_cycles;
};

/**
 * @brief Defines an instance of 6502/65C02 CPU
 */
struct cpu65x02 {
    /// CPU registers
	struct cpu65x02_regs regs;

    /// Total number of cycles executed since last reset
	uint32_t cycles;

    /// Pointer to memory load routine
	cpu65x02_load_byte_t l;

    /// Pointer to memory store routine
	cpu65x02_store_byte_t s;

    /// Pointer to optional user data that will be passed by the CPU to every
    /// call of load/store routines.
    void* user_data;

    /// Pointer to a table of all op-codes this CPU supports
    /// Will be filled by cpu6502_init()/cpu65c02_init() routine.
    const struct cpu65x02_op_handler* op_handlers;

    /// Currently handled opcode
    /// Is assigned automatically during code execution to the current
    /// opcode number.
    uint8_t opcode;

    /// Indicates whether the CPU is stopped by executing the stp instruction.
    /// This applies only to 65C02.
    uint8_t is_stopped;

    /// Indicates whether the CPU is sleeping (waits for an IRQ) after
    /// executing the wait instruction.
    /// This applies only to 65C02.
    uint8_t is_waiting;
};

#endif /* end of include guard: CPU65X02_TYPES_H_B34KHYGL */
