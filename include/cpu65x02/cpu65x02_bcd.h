/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
#ifndef CPU65X02_BCD_H_
#define CPU65X02_BCD_H_

#include "cpu65x02_types.h"
#include "cpu65x02_status.h"
#include "cpu65x02_util.h"

#ifdef CPU65X02_BCD

/**
 * @brief Adjusts the sum to valid BCD representation
 *
 * @param cpu
 * @param operand
 * @param result
 */
static inline void cpu6502_adc_decimal_adjust(struct cpu65x02* cpu,
        uint8_t operand, uint16_t* result) {
    if (CPU65X02_TEST_DEC(cpu->regs.status)) {
        // decimal mode
        uint8_t low = (cpu->regs.a & 0x0f) + (operand & 0x0f) +
            CPU65X02_GET_CARRY(cpu->regs.status);

        if(low > 0x09) {
            // bcd adjust first digit
            *result += 0x06;
        }

        if (*result > 0x99) {
            // bcd adjust second digit
            *result += 0x60;
        }
    }
}

/**
 * @brief Adjusts the difference to valid BCD representation
 *
 * @param cpu
 * @param operand
 * @param result
 */
static inline void cpu6502_sbc_decimal_adjust(struct cpu65x02* cpu,
        uint8_t operand, uint16_t* result) {
    if (CPU65X02_TEST_DEC(cpu->regs.status)) {
        // decimal mode
        uint8_t low = (cpu->regs.a & 0x0f) - (operand & 0x0f) -
            !CPU65X02_GET_CARRY(cpu->regs.status);

        if (low & 0xf0) {
            // bcd adjust first digit
            *result -= 0x06;
        }

        if (*result > 0x99) {
            // bcd adjust second digit
            *result -= 0x60;
        }
    }
}

/**
 * @brief Correct the cycle count for 65C02 adc/sbc operations in decimal mode
 *
 * @param cpu
 */
static inline void cpu65c02_op_adc_sbc_correct_cycles(struct cpu65x02* cpu) {
    if (CPU65X02_TEST_DEC(cpu->regs.status)) {
        cpu65x02_tick(cpu);
    }
}

#else
/* stubs to be optimized away */

static inline void cpu6502_adc_decimal_adjust(struct cpu65x02* cpu,
        uint8_t operand, uint16_t* result) {}
static inline void cpu6502_sbc_decimal_adjust(struct cpu65x02* cpu,
        uint8_t operand, uint16_t* result) {}
static inline void cpu65c02_op_adc_sbc_correct_cycles(struct cpu65x02* cpu) {}

#endif

#endif /* CPU65X02_BCD_H_ */
