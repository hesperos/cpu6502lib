/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

/**
 * @file This file contains all routines implementing 6502 addressing mode and
 * opcodes
 */
#ifndef PRIVATE_CPU6502_API_H_
#define PRIVATE_CPU6502_API_H_

#include <stdint.h>

/* ========================================================================== */

/**
 * @brief Implements absolute addressing mode
 *
 * Operand's address is a 16-bit instruction's argument.
 *
 * @verbatim
 *               +--+---+---+
 * Byte:         | 0|  1|  2|
 *               +--+---+---+
 * Instruction:  |OP|ADL|ADH|
 *               +--+---+---+
 *
 * Operand address:
 *                  +---+---+
 *                  |ADL|ADH|
 *                  +---+---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_abs(struct cpu65x02* cpu);

/**
 * @brief Implements absolute indexed with X, addressing mode
 *
 * Operand's base address is a 16-bit instruction's argument.
 *
 * @verbatim
 *               +--+---+---+
 * Byte:         | 0|  1|  2|
 *               +--+---+---+
 * Instruction:  |OP|ADL|ADH|
 *               +--+---+---+
 *
 * Operand address:
 *                  +---+---+
 *                  |ADL|ADH|
 *                  +---+---+
 *                + | X |
 *                  +---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_absx(struct cpu65x02* cpu);

/**
 * @brief Implements absolute indexed with Y, addressing mode
 *
 * Operand's base address is a 16-bit instruction's argument.
 *
 * @verbatim
 *               +--+---+---+
 * Byte:         | 0|  1|  2|
 *               +--+---+---+
 * Instruction:  |OP|ADL|ADH|
 *               +--+---+---+
 *
 * Operand address:
 *                  +---+---+
 *                  |ADL|ADH|
 *                  +---+---+
 *                + | Y |
 *                  +---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_absy(struct cpu65x02* cpu);

/**
 * @brief Implements zero page indexed indirect, addressing mode
 *
 * Operand's indirect base address is an 8-bit instruction's argument.
 *
 * @verbatim
 *               +--+---+
 * Byte:         | 0|  1|
 *               +--+---+
 * Instruction:  |OP| ZP|
 *               +--+---+
 *
 * Indirect address (IA):
 *               +---+---+
 *               |  0|  1|
 *               +---+---+
 *               | ZP|
 *               +---+
 *             + | X |
 *               +---+---+
 *               | IA| 0 |
 *               +---+---+
 *
 * Operand address:
 *                  +---+---+
 *                  | IA| 0 |
 *                  +---+---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_idxindx(struct cpu65x02* cpu);

/**
 * @brief Implements immediate addressing mode
 *
 * Operand itself is intruction's argument
 *
 * @verbatim
 *               +---+-------+
 *               |  0|      1|
 *               +---+-------+
 *               | OP|OPERAND|
 *               +---+-------+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_imm(struct cpu65x02* cpu);

/**
 * @brief Implements absolute indirect, addressing mode
 *
 * Operand's indirect address is a 16-bit instruction's argument.
 *
 * @verbatim
 *               +--+---+---+
 * Byte:         | 0|  1|  2|
 *               +--+---+---+
 * Instruction:  |OP|ADL|ADH|
 *               +--+---+---+
 *
 * Indirect address (IA):
 *                  +---+---+
 *                  |ADL|ADH|
 *                  +---+---+
 * Operand Address:
 *                  +---+---+
 *                  |IAL|IAH|
 *                  +---+---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_ind(struct cpu65x02* cpu);

/**
 * @brief Implements zero page indirect indexed with Y, addressing mode
 *
 * Zero page indirect base address is instruction's argument.
 *
 * @verbatim
 *               +--+---+
 * Byte:         | 0|  1|
 *               +--+---+
 * Instruction:  |OP| ZP|
 *               +--+---+
 *
 * Indirect address (IA):
 *                  +---+---+
 *                  | ZP| 0 |
 *                  +---+---+
 *
 * Operand Address:
 *                  +---+---+
 *                  |IAL|IAH|
 *                  +---+---+
 *                + | Y |
 *                  +---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_indidxy(struct cpu65x02* cpu);

/**
 * @brief Implements null addressing mode
 *
 * Always returns zero. Should be used with instructions operating with
 * implied addressing mode.
 *
 * @param cpu
 *
 * @return zero address
 */
uint16_t cpu6502_addr_null(struct cpu65x02* cpu);

/**
 * @brief Implements program counter relative, addressing mode
 *
 * The signed 8-bit relative address is instruction's argument
 *
 * @verbatim
 *               +--+---+
 * Byte:         | 0|  1|
 *               +--+---+
 * Instruction:  |OP| R |
 *               +--+---+
 *
 * New Program Counter:
 *                  +---+---+
 *                  |PCL|PCH|
 *                  +---+---+
 *                + | R |
 *                  +---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_rel(struct cpu65x02* cpu);

/**
 * @brief Implements zero page indirect, addressing mode
 *
 * Zero page indirect base address is instruction's argument.
 *
 * @verbatim
 *               +--+---+
 * Byte:         | 0|  1|
 *               +--+---+
 * Instruction:  |OP| ZP|
 *               +--+---+
 *
 * Operand Address:
 *                  +---+---+
 *                  | ZP| 0 |
 *                  +---+---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_zp(struct cpu65x02* cpu);

/**
 * @brief Implements zero page indexed with X, addressing mode
 *
 * Zero page base address is instruction's argument.
 *
 * @verbatim
 *               +--+---+
 * Byte:         | 0|  1|
 *               +--+---+
 * Instruction:  |OP| ZP|
 *               +--+---+
 *
 * Operand Address:
 *                  +---+---+
 *                  | ZP| 0 |
 *                  +---+---+
 *                + | X |
 *                  +---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_zpx(struct cpu65x02* cpu);

/**
 * @brief Implements zero page indexed with Y, addressing mode
 *
 * Zero page base address is instruction's argument.
 *
 * @verbatim
 *               +--+---+
 * Byte:         | 0|  1|
 *               +--+---+
 * Instruction:  |OP| ZP|
 *               +--+---+
 *
 * Operand Address:
 *                  +---+---+
 *                  | ZP| 0 |
 *                  +---+---+
 *                + | Y |
 *                  +---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu6502_addr_zpy(struct cpu65x02* cpu);

/* ========================================================================== */

void cpu6502_op_adc(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_and(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_asl(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_asl_acc(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_bcc(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_bcs(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_beq(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_bit(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_bmi(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_bne(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_bpl(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_brk(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_bvc(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_bvs(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_clc(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_cld(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_cli(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_clv(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_cmp(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_cpx(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_cpy(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_dec(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_dex(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_dey(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_eor(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_illegal(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_inc(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_inx(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_iny(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_jmp(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_jsr(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_lda(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_ldx(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_ldy(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_lsr(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_lsr_acc(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_nop(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_ora(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_pha(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_php(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_pla(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_plp(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_rol(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_rol_acc(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_ror(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_ror_acc(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_rti(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_rts(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_sbc(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_sec(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_sed(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_sei(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_sta(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_stx(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_sty(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_tax(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_tay(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_tsx(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_txa(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_txs(struct cpu65x02* cpu, uint16_t address);
void cpu6502_op_tya(struct cpu65x02* cpu, uint16_t address);

/* ========================================================================== */

#endif /* PRIVATE_CPU6502_API_H_ */
