/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
#ifndef PRIVATE_CPU65C02_API_H_
#define PRIVATE_CPU65C02_API_H_

#include <stdint.h>

/* ========================================================================== */

/**
 * @brief Implements zero page indirect, addressing mode
 *
 * Operand's indirect address is a zero page 8-bit instruction's
 * argument.
 *
 * @verbatim
 *               +--+---+
 * Byte:         | 0|  1|
 *               +--+---+
 * Instruction:  |OP| ZP|
 *               +--+---+
 *
 * Indirect address (IA):
 *                  +---+---+
 *                  | ZP| 0 |
 *                  +---+---+
 *
 * Operand Address:
 *                  +---+---+
 *                  |IAL|IAH|
 *                  +---+---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu65c02_addr_ind_zp(struct cpu65x02* cpu);

/**
 * @brief Implements absolute indexed indirect with X, addressing mode
 *
 * Operand's indirect base address is a 16-bit instruction's argument.
 *
 * @verbatim
 *               +--+---+---+
 * Byte:         | 0|  1|  2|
 *               +--+---+---+
 * Instruction:  |OP|ADL|ADH|
 *               +--+---+---+
 *
 * Indirect address (IA):
 *                  +---+---+
 *                  |ADL|ADH|
 *                  +---+---+
 *                + | X |
 *                  +---+
 *
 * Operand address:
 *                  +---+---+
 *                  |IAL|IAH|
 *                  +---+---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu65c02_addr_abs_indx(struct cpu65x02* cpu);

/**
 * @brief Implements absolute indexed with X, addressing mode
 *
 * Operand's base address is a 16-bit instruction's argument.
 *
 * This is re-implementation of 6502 same addressing mode with
 * additional clock cycles corrections.
 *
 * @verbatim
 *               +--+---+---+
 * Byte:         | 0|  1|  2|
 *               +--+---+---+
 * Instruction:  |OP|ADL|ADH|
 *               +--+---+---+
 *
 * Operand address:
 *                  +---+---+
 *                  |ADL|ADH|
 *                  +---+---+
 *                + | X |
 *                  +---+
 * @endverbatim
 *
 * @param cpu
 *
 * @return address
 */
uint16_t cpu65c02_addr_absx(struct cpu65x02* cpu);

/* ========================================================================== */

void cpu65c02_op_bbr0(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbr1(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbr2(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbr3(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbr4(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbr5(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbr6(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbr7(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbs0(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbs1(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbs2(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbs3(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbs4(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbs5(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbs6(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bbs7(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bit(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_bra(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_dec(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_inc(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_phx(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_phy(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_plx(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_ply(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_rmb0(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_rmb1(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_rmb2(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_rmb3(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_rmb4(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_rmb5(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_rmb6(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_rmb7(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_smb0(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_smb1(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_smb2(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_smb3(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_smb4(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_smb5(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_smb6(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_smb7(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_stp(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_stz(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_trb(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_tsb(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_wai(struct cpu65x02* cpu, uint16_t address);

/* ========================================================================== */
/* re-implemented op-codes */

void cpu65c02_op_brk(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_adc(struct cpu65x02* cpu, uint16_t address);
void cpu65c02_op_sbc(struct cpu65x02* cpu, uint16_t address);

/* ========================================================================== */

#endif /* PRIVATE_CPU65C02_API_H_ */
