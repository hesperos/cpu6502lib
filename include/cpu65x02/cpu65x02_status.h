/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
#ifndef CPU65X02_STATUS_H_KYN1N2LE
#define CPU65X02_STATUS_H_KYN1N2LE

#include "cpu65x02_bitops.h"

#include <stdint.h>

/// status register bit meanings
#define CPU65X02_STATUS_BIT_CARRY 0
#define CPU65X02_STATUS_BIT_ZERO 1
#define CPU65X02_STATUS_BIT_INT 2
#define CPU65X02_STATUS_BIT_DEC 3
#define CPU65X02_STATUS_BIT_BRK 4
#define CPU65X02_STATUS_BIT_UNUSED 5
#define CPU65X02_STATUS_BIT_OVR 6
#define CPU65X02_STATUS_BIT_SIGN 7

/// status register set ops
#define CPU65X02_SET_CARRY(__status) CPU65X02_BIT_SET(__status, CPU65X02_STATUS_BIT_CARRY)
#define CPU65X02_SET_ZERO(__status) CPU65X02_BIT_SET(__status, CPU65X02_STATUS_BIT_ZERO)
#define CPU65X02_SET_INT(__status) CPU65X02_BIT_SET(__status, CPU65X02_STATUS_BIT_INT)
#define CPU65X02_SET_DEC(__status) CPU65X02_BIT_SET(__status, CPU65X02_STATUS_BIT_DEC)
#define CPU65X02_SET_BRK(__status) CPU65X02_BIT_SET(__status, CPU65X02_STATUS_BIT_BRK)
#define CPU65X02_SET_UNUSED(__status) CPU65X02_BIT_SET(__status, CPU65X02_STATUS_BIT_UNUSED)
#define CPU65X02_SET_OVR(__status) CPU65X02_BIT_SET(__status, CPU65X02_STATUS_BIT_OVR)
#define CPU65X02_SET_SIGN(__status) CPU65X02_BIT_SET(__status, CPU65X02_STATUS_BIT_SIGN)

/// status register get ops
#define CPU65X02_GET_CARRY(__status) CPU65X02_BIT_GET(__status, CPU65X02_STATUS_BIT_CARRY)
#define CPU65X02_GET_ZERO(__status) CPU65X02_BIT_GET(__status, CPU65X02_STATUS_BIT_ZERO)
#define CPU65X02_GET_INT(__status) CPU65X02_BIT_GET(__status, CPU65X02_STATUS_BIT_INT)
#define CPU65X02_GET_DEC(__status) CPU65X02_BIT_GET(__status, CPU65X02_STATUS_BIT_DEC)
#define CPU65X02_GET_BRK(__status) CPU65X02_BIT_GET(__status, CPU65X02_STATUS_BIT_BRK)
#define CPU65X02_GET_OVR(__status) CPU65X02_BIT_GET(__status, CPU65X02_STATUS_BIT_OVR)
#define CPU65X02_GET_SIGN(__status) CPU65X02_BIT_GET(__status, CPU65X02_STATUS_BIT_SIGN)

/// status register get ops
#define CPU65X02_TEST_CARRY(__status) CPU65X02_BIT_TEST(__status, CPU65X02_STATUS_BIT_CARRY)
#define CPU65X02_TEST_ZERO(__status) CPU65X02_BIT_TEST(__status, CPU65X02_STATUS_BIT_ZERO)
#define CPU65X02_TEST_INT(__status) CPU65X02_BIT_TEST(__status, CPU65X02_STATUS_BIT_INT)
#define CPU65X02_TEST_DEC(__status) CPU65X02_BIT_TEST(__status, CPU65X02_STATUS_BIT_DEC)
#define CPU65X02_TEST_BRK(__status) CPU65X02_BIT_TEST(__status, CPU65X02_STATUS_BIT_BRK)
#define CPU65X02_TEST_OVR(__status) CPU65X02_BIT_TEST(__status, CPU65X02_STATUS_BIT_OVR)
#define CPU65X02_TEST_SIGN(__status) CPU65X02_BIT_TEST(__status, CPU65X02_STATUS_BIT_SIGN)

/// status register clear ops
#define CPU65X02_CLEAR_CARRY(__status) CPU65X02_BIT_CLEAR(__status, CPU65X02_STATUS_BIT_CARRY)
#define CPU65X02_CLEAR_ZERO(__status) CPU65X02_BIT_CLEAR(__status, CPU65X02_STATUS_BIT_ZERO)
#define CPU65X02_CLEAR_INT(__status) CPU65X02_BIT_CLEAR(__status, CPU65X02_STATUS_BIT_INT)
#define CPU65X02_CLEAR_DEC(__status) CPU65X02_BIT_CLEAR(__status, CPU65X02_STATUS_BIT_DEC)
#define CPU65X02_CLEAR_BRK(__status) CPU65X02_BIT_CLEAR(__status, CPU65X02_STATUS_BIT_BRK)
#define CPU65X02_CLEAR_OVR(__status) CPU65X02_BIT_CLEAR(__status, CPU65X02_STATUS_BIT_OVR)
#define CPU65X02_CLEAR_SIGN(__status) CPU65X02_BIT_CLEAR(__status, CPU65X02_STATUS_BIT_SIGN)

// forward declaration
struct cpu65x02;

/**
 * @brief Calculates zero bit from given value
 *
 * @param cpu CPU instance
 * @param result operations result
 */
void cpu65x02_calc_zero(struct cpu65x02* cpu, uint8_t result);

/**
 * @brief Calculates sign bit from given value
 *
 * @param cpu CPU instance
 * @param result operations result
 */
void cpu65x02_calc_sign(struct cpu65x02* cpu, uint8_t result);

/**
 * @brief Calculates zero bit from accumulator
 *
 * @param cpu CPU instance
 * @param result operations result
 */
void cpu65x02_calc_zero_acc(struct cpu65x02* cpu);

/**
 * @brief Calculates sign bit from accumulator
 *
 * @param cpu CPU instance
 * @param result operations result
 */
void cpu65x02_calc_sign_acc(struct cpu65x02* cpu);

/**
 * @brief Calculates overflow bit
 *
 * Should be used in context of addition operations
 *
 * @param cpu CPU instance
 * @param a operand A
 * @param b operand B
 * @param result sum of A, B
 */
void cpu65x02_calc_ovr(struct cpu65x02* cpu,
        uint8_t a, uint8_t b,
        uint16_t result);

/**
 * @brief Calculates overflow bit
 *
 * Should be used in context of substraction operations
 *
 * @param cpu CPU instance
 * @param a operand A
 * @param b operand B
 * @param result difference of A, B
 */
void cpu65x02_calc_und(struct cpu65x02* cpu,
        uint8_t a, uint8_t b,
        uint16_t result);

/**
 * @brief Calculates carry bit
 *
 * Should be used in context of addition operations
 *
 * @param cpu CPU instance
 */
void cpu65x02_calc_carry(struct cpu65x02* cpu, uint16_t result);

/**
 * @brief Calculates carry bit
 *
 * Should be used in context of substraction operations
 *
 * @param cpu CPU instance
 */
void cpu65x02_calc_borrow(struct cpu65x02* cpu, uint16_t result);

#endif /* end of include guard: CPU65X02_STATUS_H_KYN1N2LE */

