/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

/**
 * @mainpage Summary
 *
 * This project provides an implementation of 65x02 CPU in a form of a static
 * library. Both NMOS and CMOS cpu variants are supported.
 *
 * @section building Building
 *
 * Debug build
 *
 * @code
 * $ cmake -DCMAKE_BUILD_TYPE=Debug ..
 * @endcode
 *
 * Release build
 *
 * @code
 * $ cmake -DCMAKE_BUILD_TYPE=Release ..
 * @endcode
 *
 * Benchmarks
 *
 * @code
 * $ cmake -DCMAKE_BUILD_TYPE=Release -DBENCHMARK=ON ..
 * @endcode
 *
 * @section api API
 *
 * The API is divided into three distinct sets. There are three API sets:
 * - NMOS specific (prefixed as cpu6502_) - contains routines specific to NMOS
 *   CPU version,
 * - CMOS specific (prefixed as cpu65c02_) - contains additional routines for
 *   CMOS CPU version,
 * - common (prefixed as cpu65x02_) - routines shared between NMOS and CMOS
 *   versions.
 *
 * The public API is comprised of:
 * - cpu6502_init() / cpu65c02_init() - Should be used to initialize the
 *   instance of the CPU,
 * - cpu6502_reset() / cpu65c02_reset() - Should be used to reset the CPU just
 *   after it has been initialized,
 * - cpu65x02_step() - Executes a single instruction in the CPU's pipeline,
 *   should be called periodically in a busy loop or timer,
 * - cpu65x02_irq() - Triggers an interrupt, the CPU will save its context and
 *   jump to IRQ handler
 * - cpu65x02_nmi() - Triggers non-maskable interrupt
 * - cpu65x02_dump_core() - Dumps the core's execution context to stderr.
 *   Resolves to an empty stub in Release mode.
 */
#ifndef CPU65X02_H_B34KHYGL
#define CPU65X02_H_B34KHYGL

#ifdef __cplusplus
extern "C" {
#endif

#include "cpu65x02_types.h"

#include "cpu6502_api.h"
#include "cpu65c02_api.h"
#include "cpu65x02_api.h"

#include "cpu65x02_debug.h"

#ifdef __cplusplus
}
#endif

#endif /* end of include guard: CPU65X02_H_B34KHYGL */

