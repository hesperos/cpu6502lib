/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
#ifndef CPU65X02_CONSTANTS_H_
#define CPU65X02_CONSTANTS_H_

/// Contains the low byte of the NMI handler routine address
#define CPU65X02_NMI_VECTOR 0xfffa

/// Contains the low byte of the RESET handler routine address
#define CPU65X02_RESET_VECTOR 0xfffc

/// Contains the low byte of the IRQ handler routine address
#define CPU65X02_IRQ_VECTOR 0xfffe

/// Base address of the STACK
#define CPU65X02_STACK_BASE 0x0100

/// Page size
#define CPU65X02_PAGE_SIZE 0x100

/// Maximum number of op-codes, CPU supports
#define CPU65X02_MAX_OPCODES 0x100

#endif /* CPU65X02_CONSTANTS_H_ */
