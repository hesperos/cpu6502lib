/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef CPU65X02_BITOPS_H_
#define CPU65X02_BITOPS_H_

/// Sets selected bit in a byte
#define CPU65X02_BIT_SET(__byte, __bit) \
	(__byte |= (0x01 << __bit))

/// Gets the value (0 or 1) of selected bit from a byte
#define CPU65X02_BIT_GET(__byte, __bit) \
    ((__byte >> __bit) & 0x01)

/// Clears selected bit in a byte
#define CPU65X02_BIT_CLEAR(__byte, __bit) \
	(__byte &= ~(0x01 << __bit))

/// Checks if bit is set (0 if not set, anything else if set)
#define CPU65X02_BIT_TEST(__byte, __bit) \
	(__byte & (0x01 << __bit))

/// Toggles the bit's value
#define CPU65X02_BIT_TOGGLE(__byte, __bit) \
	(__byte ^= (0x01 << __bit))

#endif /* CPU65X02_BITOPS_H_ */
