/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
#ifndef CPU65X02_API_H_B34KHYGL
#define CPU65X02_API_H_B34KHYGL

#include "cpu65x02_types.h"

/**
 * @brief Executes a single instruction
 *
 * Fetches an instruction, associated data and executes it
 *
 * @param cpu Pointer to the CPU instance
 */
void cpu65x02_step(struct cpu65x02* cpu);

/**
 * @brief Force an interrupt
 *
 * The cpu will push its current context to the stack and jump to the
 * CPU65X02_IRQ_VECTOR vector, if interrupts are enabled.
 *
 * @param cpu Pointer to the CPU instance
 */
void cpu65x02_irq(struct cpu65x02* cpu);

/**
 * @brief Force a non-maskable interrupt
 *
 * The cpu will push its current context to the stack and jump to the
 * CPU65X02_NMI_VECTOR vector.
 *
 * @param cpu Pointer to the CPU instance
 */
void cpu65x02_nmi(struct cpu65x02* cpu);

#endif /* end of include guard: CPU65X02_API_H_B34KHYGL */

