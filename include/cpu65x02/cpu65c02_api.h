/* Copyright (C)
 * 2017 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
#ifndef CPU65C02_API_H_
#define CPU65C02_API_H_

#include "cpu65x02_types.h"

/**
 * @brief Initializes the CPU instance as 65C02 CPU
 *
 * Should be called just after allocating the instance of struct cpu65x02, to
 * initialize it to known state and populate necessary values.
 *
 * @param cpu Pointer to the CPU instance
 * @param l Pointer to load routine
 * @param s Pointer to store routine
 * @param user_data Optional user data (NULL if unused)
 */
void cpu65c02_init(struct cpu65x02* cpu,
        cpu65x02_load_byte_t l,
        cpu65x02_store_byte_t s,
        void* user_data);

/**
 * @brief Re-sets the CPU
 *
 * Clears all CPU registers and restores their default values and reloads the
 * RESET vector. Should be called just before starting to execute any code.
 *
 * @param cpu Pointer to the CPU instance
 */
void cpu65c02_reset(struct cpu65x02* cpu);

#endif /* CPU65C02_API_H_ */
