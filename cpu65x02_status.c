#include "include/cpu65x02/cpu65x02_status.h"
#include "include/cpu65x02/cpu65x02_types.h"

void cpu65x02_calc_zero(struct cpu65x02* cpu, uint8_t result) {
    if (result) {
        CPU65X02_CLEAR_ZERO(cpu->regs.status);
    }
    else {
        CPU65X02_SET_ZERO(cpu->regs.status);
    }
}

void cpu65x02_calc_sign(struct cpu65x02* cpu, uint8_t result) {
    if (result & 0x80) {
        CPU65X02_SET_SIGN(cpu->regs.status);
    }
    else {
        CPU65X02_CLEAR_SIGN(cpu->regs.status);
    }
}

void cpu65x02_calc_zero_acc(struct cpu65x02* cpu) {
    cpu65x02_calc_zero(cpu, cpu->regs.a);
}

void cpu65x02_calc_sign_acc(struct cpu65x02* cpu) {
    cpu65x02_calc_sign(cpu, cpu->regs.a);
}

static void cpu65x02_calc_ovr_common(struct cpu65x02* cpu,
        uint8_t a, uint8_t b, uint16_t result, uint8_t inverse) {

    uint8_t p1 = ((a ^ b) & 0x80);
    if (inverse) {
        p1 = !p1;
    }

    if (p1 && ((a ^ result) & 0x80)) {
        CPU65X02_SET_OVR(cpu->regs.status);
    }
    else {
        CPU65X02_CLEAR_OVR(cpu->regs.status);
    }
}

void cpu65x02_calc_ovr(struct cpu65x02* cpu,
        uint8_t a, uint8_t b,
        uint16_t result) {
    cpu65x02_calc_ovr_common(cpu, a, b, result, 1);
}

void cpu65x02_calc_und(struct cpu65x02* cpu,
        uint8_t a, uint8_t b,
        uint16_t result) {
    cpu65x02_calc_ovr_common(cpu, a, b, result, 0);
}

void cpu65x02_calc_carry(struct cpu65x02* cpu, uint16_t result) {
    if (CPU65X02_TEST_DEC(cpu->regs.status)) {
        if (result > 0x99) {
            CPU65X02_SET_CARRY(cpu->regs.status);
        }
        else {
            CPU65X02_CLEAR_CARRY(cpu->regs.status);
        }
    }
    else {
        if (result & 0xff00) {
            CPU65X02_SET_CARRY(cpu->regs.status);
        }
        else {
            CPU65X02_CLEAR_CARRY(cpu->regs.status);
        }
    }
}

void cpu65x02_calc_borrow(struct cpu65x02* cpu, uint16_t result) {
    if (result < 0x0100) {
        CPU65X02_SET_CARRY(cpu->regs.status);
    }
    else {
        CPU65X02_CLEAR_CARRY(cpu->regs.status);
    }
}
