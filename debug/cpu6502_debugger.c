#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include <cpu65x02/cpu65x02.h>

#define MEMORY_SIZE 0x10000
uint8_t memory[MEMORY_SIZE] = {0x00};
const char* binary_name = NULL;

uint8_t load(uint16_t address, void* user_data) {
    uint8_t value = memory[address];
    fprintf(stderr, "  <-- %02x@%04x\n", value, address);
    return value;
}

void store(uint16_t address, uint8_t value, void* user_data) {
    fprintf(stderr, "  --> %02x@%04x\n", value, address);
    memory[address] = value;
}

int load_rom(const char* rom_path, uint16_t offset) {
    FILE *f = fopen(rom_path, "rb");
    if (!f) {
        fprintf(stderr, "Unable to load rom %s\n", rom_path);
        return 1;
    }

    const size_t chunk = 1024;
    const size_t max_bytes = MEMORY_SIZE - offset;
    size_t read_bytes = 0;
    size_t read_total = 0;
    size_t to_read = chunk < max_bytes ? chunk : max_bytes;
    uint8_t* ptr = &memory[offset];

    while (!feof(f) && (read_bytes = fread(ptr, 1, to_read, f))) {
        read_total += read_bytes;
        ptr += read_bytes;
        if (read_total >= max_bytes) {
            break;
        }
        size_t mem_left = max_bytes - read_total;
        to_read = chunk < mem_left ? chunk : mem_left;
    }

    fclose(f);
    fprintf(stdout, "Loaded: %u bytes\n", read_total);
    return 0;
}

void usage() {
    fprintf(stderr, "%s OPTIONS\n"
            "Options:\n"
            "\t-r <rom>\n"
            "\t-C - CPU is CMOS version\n"
            "\t-o <offset|0>\n"
            "\t-p <program counter|default>\n"
            "\t-c <max_cycles|0>\n",
            binary_name);
}

extern char* optarg;

int main(int argc, char *argv[]) {
	struct cpu65x02 cpu;
    uint16_t offset = 0;
    uint32_t max_cycles = 0;
    uint16_t pc = 0xff;

    int opt;
    char rom_path[1024] = {0x00};
    binary_name = argv[0];

    int is_cmos = 0;
    int is_pc_given = 0;

    while (-1 != (opt = getopt(argc, argv, "r:o:c:Cp:h"))) {
        switch (opt) {
            case 'r':
                strncpy(rom_path, optarg, sizeof(rom_path));
                break;

            case 'o':
                offset = atoi(optarg);
                offset = strtoul(optarg, NULL, 10);
                break;

            case 'c':
                max_cycles = strtoul(optarg, NULL, 10);
                break;

            case 'p':
                pc = strtoul(optarg, NULL, 10);
                is_pc_given = 1;
                break;

            case 'C':
                is_cmos = 1;
                break;

            case 'h':
            default:
                usage();
                exit(EXIT_SUCCESS);
                break;
        }
    }

    if (strlen(rom_path) < 1) {
        fprintf(stderr, "No ROM path provided\n");
        usage();
        exit(EXIT_FAILURE);
    }

    fprintf(stdout,
            "Loading ROM: %s @ %04x, max_cycles: %u\n",
            rom_path, offset, max_cycles);
    if (load_rom(rom_path, offset)) {
        exit(EXIT_FAILURE);
    }

    if (is_cmos) {
        cpu65c02_init(&cpu, load, store, NULL);
        cpu65c02_reset(&cpu);
    }
    else {
        cpu6502_init(&cpu, load, store, NULL);
        cpu6502_reset(&cpu);
    }

    if (is_pc_given) {
        cpu.regs.pc = pc;
    }

    fprintf(stdout, "Executing from: 0x%04x\n", cpu.regs.pc);

    for(;;) {
        cpu65x02_step(&cpu);

        if (max_cycles && cpu.cycles > max_cycles) {
            cpu65x02_dump_core(&cpu);
            break;
        }
    }
    return 0;
}
