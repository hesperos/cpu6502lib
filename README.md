# cpu6502lib

Contains an implementation of NMOS 6502 and CMOS 65C02 in form of a library.

# Building

Standard cmake build:

    $ mkdir bld && cd bld
    $ cmake -DCMAKE_BUILD_TYPE=Release [OPTIONS] ..
    $ make && make install

Options are:
 - BCD (ON by default) - support for decimal mode
 - CMOS (ON by default) - support for 65C02 extended instruction set
 - BENCHMARK (OFF by default) - builds google benchmark and benchmarks
 themselves. These are useful during development to measure if the introduced
 change has brought more complexity or not.

Example of building with options:

    $ mkdir bld && cd bld
    $ cmake -DCMAKE_BUILD_TYPE=Release -DBCD=OFF -DBENCHMARK=ON ..
    $ make && make install

# Benchmarks

In the build directory the benchmarks are build as a standalone self-contained
programs. They can be executed from the benchmarks subdirectory:

    $ cd bld
    $ make
    $ benchmarks/cpu6502_test_suite_benchmark

The location is important since the benchmark will try to load a binary rom
image for which a relative path is used.

# API

The API set is very small. Just declare an instance of the CPU, initialize it
with `cpu6502_init` function provided, reset and execute instructions. Two IO
routines are required for operations on memory.

Detailed doxygen documentation can be found
[here](https://dagon666.github.io/cpu6502lib/).
