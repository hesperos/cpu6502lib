#include "include/cpu65x02/cpu6502_api.h"
#include "include/cpu65x02/cpu65c02_api.h"
#include "include/cpu65x02/cpu65x02_bcd.h"
#include "include/cpu65x02/cpu65x02_constants.h"
#include "include/cpu65x02/cpu65x02_status.h"
#include "include/cpu65x02/cpu65x02_util.h"

#include "include/cpu65x02/private/cpu6502_api.h"
#include "include/cpu65x02/private/cpu65c02_api.h"

#include <string.h>

/* ========================================================================== */

static struct cpu65x02_op_handler cpu65c02_op_handlers[CPU65X02_MAX_OPCODES] = {
    { cpu6502_addr_null,      cpu65c02_op_brk,     7 }, // 0x00
    { cpu6502_addr_idxindx,   cpu6502_op_ora,      6 }, // 0x01
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x02
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x03
    { cpu6502_addr_zp,        cpu65c02_op_tsb,     5 }, // 0x04
    { cpu6502_addr_zp,        cpu6502_op_ora,      3 }, // 0x05
    { cpu6502_addr_zp,        cpu6502_op_asl,      5 }, // 0x06
    { cpu6502_addr_zp,        cpu65c02_op_rmb0,    5 }, // 0x07
    { cpu6502_addr_null,      cpu6502_op_php,      3 }, // 0x08
    { cpu6502_addr_imm,       cpu6502_op_ora,      2 }, // 0x09
    { cpu6502_addr_null,      cpu6502_op_asl_acc,  2 }, // 0x0a
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x0b
    { cpu6502_addr_abs,       cpu65c02_op_tsb,     6 }, // 0x0c
    { cpu6502_addr_abs,       cpu6502_op_ora,      4 }, // 0x0d
    { cpu6502_addr_abs,       cpu6502_op_asl,      6 }, // 0x0e
    { cpu6502_addr_zp,        cpu65c02_op_bbr0,    5 }, // 0x0f
    { cpu6502_addr_rel,       cpu6502_op_bpl,      2 }, // 0x10
    { cpu6502_addr_indidxy,   cpu6502_op_ora,      5 }, // 0x11
    { cpu65c02_addr_ind_zp,   cpu6502_op_ora,      5 }, // 0x12
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x13
    { cpu6502_addr_zp,        cpu65c02_op_trb,     5 }, // 0x14
    { cpu6502_addr_zpx,       cpu6502_op_ora,      4 }, // 0x15
    { cpu6502_addr_zpx,       cpu6502_op_asl,      6 }, // 0x16
    { cpu6502_addr_zp,        cpu65c02_op_rmb1,    5 }, // 0x17
    { cpu6502_addr_null,      cpu6502_op_clc,      2 }, // 0x18
    { cpu6502_addr_absy,      cpu6502_op_ora,      4 }, // 0x19
    { cpu6502_addr_null,      cpu65c02_op_inc,     2 }, // 0x1a
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x1b
    { cpu6502_addr_abs,       cpu65c02_op_trb,     6 }, // 0x1c
    { cpu6502_addr_absx,      cpu6502_op_ora,      4 }, // 0x1d
    { cpu65c02_addr_absx,     cpu6502_op_asl,      6 }, // 0x1e
    { cpu6502_addr_zp,        cpu65c02_op_bbr1,    5 }, // 0x1f
    { cpu6502_addr_abs,       cpu6502_op_jsr,      6 }, // 0x20
    { cpu6502_addr_idxindx,   cpu6502_op_and,      6 }, // 0x21
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x22
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x23
    { cpu6502_addr_zp,        cpu6502_op_bit,      3 }, // 0x24
    { cpu6502_addr_zp,        cpu6502_op_and,      3 }, // 0x25
    { cpu6502_addr_zp,        cpu6502_op_rol,      5 }, // 0x26
    { cpu6502_addr_zp,        cpu65c02_op_rmb2,    5 }, // 0x27
    { cpu6502_addr_null,      cpu6502_op_plp,      4 }, // 0x28
    { cpu6502_addr_imm,       cpu6502_op_and,      2 }, // 0x29
    { cpu6502_addr_null,      cpu6502_op_rol_acc,  2 }, // 0x2a
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x2b
    { cpu6502_addr_abs,       cpu6502_op_bit,      4 }, // 0x2c
    { cpu6502_addr_abs,       cpu6502_op_and,      4 }, // 0x2d
    { cpu6502_addr_abs,       cpu6502_op_rol,      6 }, // 0x2e
    { cpu6502_addr_zp,        cpu65c02_op_bbr2,    5 }, // 0x2f
    { cpu6502_addr_rel,       cpu6502_op_bmi,      2 }, // 0x30
    { cpu6502_addr_indidxy,   cpu6502_op_and,      5 }, // 0x31
    { cpu65c02_addr_ind_zp,   cpu6502_op_and,      5 }, // 0x32
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x33
    { cpu6502_addr_zpx,       cpu65c02_op_bit,     4 }, // 0x34
    { cpu6502_addr_zpx,       cpu6502_op_and,      4 }, // 0x35
    { cpu6502_addr_zpx,       cpu6502_op_rol,      6 }, // 0x36
    { cpu6502_addr_zp,        cpu65c02_op_rmb3,    5 }, // 0x37
    { cpu6502_addr_null,      cpu6502_op_sec,      2 }, // 0x38
    { cpu6502_addr_absy,      cpu6502_op_and,      4 }, // 0x39
    { cpu6502_addr_null,      cpu65c02_op_dec,     2 }, // 0x3a
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x3b
    { cpu6502_addr_absx,      cpu65c02_op_bit,     4 }, // 0x3c
    { cpu6502_addr_absx,      cpu6502_op_and,      4 }, // 0x3d
    { cpu65c02_addr_absx,     cpu6502_op_rol,      6 }, // 0x3e
    { cpu6502_addr_zp,        cpu65c02_op_bbr3,    5 }, // 0x3f
    { cpu6502_addr_null,      cpu6502_op_rti,      6 }, // 0x40
    { cpu6502_addr_idxindx,   cpu6502_op_eor,      6 }, // 0x41
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x42
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x43
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x44
    { cpu6502_addr_zp,        cpu6502_op_eor,      3 }, // 0x45
    { cpu6502_addr_zp,        cpu6502_op_lsr,      5 }, // 0x46
    { cpu6502_addr_zp,        cpu65c02_op_rmb4,    5 }, // 0x47
    { cpu6502_addr_null,      cpu6502_op_pha,      3 }, // 0x48
    { cpu6502_addr_imm,       cpu6502_op_eor,      2 }, // 0x49
    { cpu6502_addr_null,      cpu6502_op_lsr_acc,  2 }, // 0x4a
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x4b
    { cpu6502_addr_abs,       cpu6502_op_jmp,      3 }, // 0x4c
    { cpu6502_addr_abs,       cpu6502_op_eor,      4 }, // 0x4d
    { cpu6502_addr_abs,       cpu6502_op_lsr,      6 }, // 0x4e
    { cpu6502_addr_zp,        cpu65c02_op_bbr4,    5 }, // 0x4f
    { cpu6502_addr_rel,       cpu6502_op_bvc,      2 }, // 0x50
    { cpu6502_addr_indidxy,   cpu6502_op_eor,      5 }, // 0x51
    { cpu65c02_addr_ind_zp,   cpu6502_op_eor,      5 }, // 0x52
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x53
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x54
    { cpu6502_addr_zpx,       cpu6502_op_eor,      4 }, // 0x55
    { cpu6502_addr_zpx,       cpu6502_op_lsr,      6 }, // 0x56
    { cpu6502_addr_zp,        cpu65c02_op_rmb5,    5 }, // 0x57
    { cpu6502_addr_null,      cpu6502_op_cli,      2 }, // 0x58
    { cpu6502_addr_absy,      cpu6502_op_eor,      4 }, // 0x59
    { cpu6502_addr_null,      cpu65c02_op_phy,     3 }, // 0x5a
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x5b
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x5c
    { cpu6502_addr_absx,      cpu6502_op_eor,      4 }, // 0x5d
    { cpu65c02_addr_absx,     cpu6502_op_lsr,      6 }, // 0x5e
    { cpu6502_addr_zp,        cpu65c02_op_bbr5,    5 }, // 0x5f
    { cpu6502_addr_null,      cpu6502_op_rts,      6 }, // 0x60
    { cpu6502_addr_idxindx,   cpu65c02_op_adc,     6 }, // 0x61
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x62
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x63
    { cpu6502_addr_imm,       cpu65c02_op_stz,     3 }, // 0x64
    { cpu6502_addr_zp,        cpu65c02_op_adc,     3 }, // 0x65
    { cpu6502_addr_zp,        cpu6502_op_ror,      5 }, // 0x66
    { cpu6502_addr_zp,        cpu65c02_op_rmb6,    5 }, // 0x67
    { cpu6502_addr_null,      cpu6502_op_pla,      4 }, // 0x68
    { cpu6502_addr_imm,       cpu65c02_op_adc,     2 }, // 0x69
    { cpu6502_addr_null,      cpu6502_op_ror_acc,  2 }, // 0x6a
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x6b
    { cpu6502_addr_ind,       cpu6502_op_jmp,      5 }, // 0x6c
    { cpu6502_addr_abs,       cpu65c02_op_adc,     4 }, // 0x6d
    { cpu6502_addr_abs,       cpu6502_op_ror,      6 }, // 0x6e
    { cpu6502_addr_zp,        cpu65c02_op_bbr6,    5 }, // 0x6f
    { cpu6502_addr_rel,       cpu6502_op_bvs,      2 }, // 0x70
    { cpu6502_addr_indidxy,   cpu65c02_op_adc,     5 }, // 0x71
    { cpu65c02_addr_ind_zp,   cpu65c02_op_adc,     5 }, // 0x72
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x73
    { cpu6502_addr_zpx,       cpu65c02_op_stz,     4 }, // 0x74
    { cpu6502_addr_zpx,       cpu65c02_op_adc,     4 }, // 0x75
    { cpu6502_addr_zpx,       cpu6502_op_ror,      6 }, // 0x76
    { cpu6502_addr_zp,        cpu65c02_op_rmb7,    5 }, // 0x77
    { cpu6502_addr_null,      cpu6502_op_sei,      2 }, // 0x78
    { cpu6502_addr_absy,      cpu65c02_op_adc,     4 }, // 0x79
    { cpu6502_addr_null,      cpu65c02_op_ply,     4 }, // 0x7a
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x7b
    { cpu65c02_addr_abs_indx, cpu6502_op_jmp,      6 }, // 0x7c
    { cpu6502_addr_absx,      cpu65c02_op_adc,     4 }, // 0x7d
    { cpu65c02_addr_absx,     cpu6502_op_ror,      6 }, // 0x7e
    { cpu6502_addr_zp,        cpu65c02_op_bbr7,    5 }, // 0x7f
    { cpu6502_addr_rel,       cpu65c02_op_bra,     3 }, // 0x80
    { cpu6502_addr_idxindx,   cpu6502_op_sta,      6 }, // 0x81
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x82
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x83
    { cpu6502_addr_zp,        cpu6502_op_sty,      3 }, // 0x84
    { cpu6502_addr_zp,        cpu6502_op_sta,      3 }, // 0x85
    { cpu6502_addr_zp,        cpu6502_op_stx,      3 }, // 0x86
    { cpu6502_addr_zp,        cpu65c02_op_smb0,    5 }, // 0x87
    { cpu6502_addr_null,      cpu6502_op_dey,      2 }, // 0x88
    { cpu6502_addr_imm,       cpu65c02_op_bit,     2 }, // 0x89
    { cpu6502_addr_null,      cpu6502_op_txa,      2 }, // 0x8a
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x8b
    { cpu6502_addr_abs,       cpu6502_op_sty,      4 }, // 0x8c
    { cpu6502_addr_abs,       cpu6502_op_sta,      4 }, // 0x8d
    { cpu6502_addr_abs,       cpu6502_op_stx,      4 }, // 0x8e
    { cpu6502_addr_zp,        cpu65c02_op_bbs0,    5 }, // 0x8f
    { cpu6502_addr_rel,       cpu6502_op_bcc,      2 }, // 0x90
    { cpu6502_addr_indidxy,   cpu6502_op_sta,      6 }, // 0x91
    { cpu65c02_addr_ind_zp,   cpu6502_op_sta,      5 }, // 0x92
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x93
    { cpu6502_addr_zpx,       cpu6502_op_sty,      4 }, // 0x94
    { cpu6502_addr_zpx,       cpu6502_op_sta,      4 }, // 0x95
    { cpu6502_addr_zpy,       cpu6502_op_stx,      4 }, // 0x96
    { cpu6502_addr_zp,        cpu65c02_op_smb1,    5 }, // 0x97
    { cpu6502_addr_null,      cpu6502_op_tya,      2 }, // 0x98
    { cpu6502_addr_absy,      cpu6502_op_sta,      5 }, // 0x99
    { cpu6502_addr_null,      cpu6502_op_txs,      2 }, // 0x9a
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0x9b
    { cpu6502_addr_abs,       cpu65c02_op_stz,     4 }, // 0x9c
    { cpu6502_addr_absx,      cpu6502_op_sta,      5 }, // 0x9d
    { cpu6502_addr_absx,      cpu65c02_op_stz,     5 }, // 0x9e
    { cpu6502_addr_zp,        cpu65c02_op_bbs1,    5 }, // 0x9f
    { cpu6502_addr_imm,       cpu6502_op_ldy,      2 }, // 0xa0
    { cpu6502_addr_idxindx,   cpu6502_op_lda,      6 }, // 0xa1
    { cpu6502_addr_imm,       cpu6502_op_ldx,      2 }, // 0xa2
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xa3
    { cpu6502_addr_zp,        cpu6502_op_ldy,      3 }, // 0xa4
    { cpu6502_addr_zp,        cpu6502_op_lda,      3 }, // 0xa5
    { cpu6502_addr_zp,        cpu6502_op_ldx,      3 }, // 0xa6
    { cpu6502_addr_zp,        cpu65c02_op_smb2,    5 }, // 0xa7
    { cpu6502_addr_null,      cpu6502_op_tay,      2 }, // 0xa8
    { cpu6502_addr_imm,       cpu6502_op_lda,      2 }, // 0xa9
    { cpu6502_addr_null,      cpu6502_op_tax,      2 }, // 0xaa
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xab
    { cpu6502_addr_abs,       cpu6502_op_ldy,      4 }, // 0xac
    { cpu6502_addr_abs,       cpu6502_op_lda,      4 }, // 0xad
    { cpu6502_addr_abs,       cpu6502_op_ldx,      4 }, // 0xae
    { cpu6502_addr_zp,        cpu65c02_op_bbs2,    5 }, // 0xaf
    { cpu6502_addr_rel,       cpu6502_op_bcs,      2 }, // 0xb0
    { cpu6502_addr_indidxy,   cpu6502_op_lda,      5 }, // 0xb1
    { cpu65c02_addr_ind_zp,   cpu6502_op_lda,      5 }, // 0xb2
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xb3
    { cpu6502_addr_zpx,       cpu6502_op_ldy,      4 }, // 0xb4
    { cpu6502_addr_zpx,       cpu6502_op_lda,      4 }, // 0xb5
    { cpu6502_addr_zpy,       cpu6502_op_ldx,      4 }, // 0xb6
    { cpu6502_addr_zp,        cpu65c02_op_smb3,    5 }, // 0xb7
    { cpu6502_addr_null,      cpu6502_op_clv,      2 }, // 0xb8
    { cpu6502_addr_absy,      cpu6502_op_lda,      4 }, // 0xb9
    { cpu6502_addr_null,      cpu6502_op_tsx,      2 }, // 0xba
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xbb
    { cpu6502_addr_absx,      cpu6502_op_ldy,      4 }, // 0xbc
    { cpu6502_addr_absx,      cpu6502_op_lda,      4 }, // 0xbd
    { cpu6502_addr_absy,      cpu6502_op_ldx,      4 }, // 0xbe
    { cpu6502_addr_zp,        cpu65c02_op_bbs3,    5 }, // 0xbf
    { cpu6502_addr_imm,       cpu6502_op_cpy,      2 }, // 0xc0
    { cpu6502_addr_idxindx,   cpu6502_op_cmp,      6 }, // 0xc1
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xc2
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xc3
    { cpu6502_addr_zp,        cpu6502_op_cpy,      3 }, // 0xc4
    { cpu6502_addr_zp,        cpu6502_op_cmp,      3 }, // 0xc5
    { cpu6502_addr_zp,        cpu6502_op_dec,      5 }, // 0xc6
    { cpu6502_addr_zp,        cpu65c02_op_smb4,    5 }, // 0xc7
    { cpu6502_addr_null,      cpu6502_op_iny,      2 }, // 0xc8
    { cpu6502_addr_imm,       cpu6502_op_cmp,      2 }, // 0xc9
    { cpu6502_addr_null,      cpu6502_op_dex,      2 }, // 0xca
    { cpu6502_addr_null,      cpu65c02_op_wai,     3 }, // 0xcb
    { cpu6502_addr_abs,       cpu6502_op_cpy,      4 }, // 0xcc
    { cpu6502_addr_abs,       cpu6502_op_cmp,      4 }, // 0xcd
    { cpu6502_addr_abs,       cpu6502_op_dec,      6 }, // 0xce
    { cpu6502_addr_zp,        cpu65c02_op_bbs4,    5 }, // 0xcf
    { cpu6502_addr_rel,       cpu6502_op_bne,      2 }, // 0xd0
    { cpu6502_addr_indidxy,   cpu6502_op_cmp,      5 }, // 0xd1
    { cpu65c02_addr_ind_zp,   cpu6502_op_cmp,      5 }, // 0xd2
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xd3
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xd4
    { cpu6502_addr_zpx,       cpu6502_op_cmp,      4 }, // 0xd5
    { cpu6502_addr_zpx,       cpu6502_op_dec,      6 }, // 0xd6
    { cpu6502_addr_zp,        cpu65c02_op_smb5,    5 }, // 0xd7
    { cpu6502_addr_null,      cpu6502_op_cld,      2 }, // 0xd8
    { cpu6502_addr_absy,      cpu6502_op_cmp,      4 }, // 0xd9
    { cpu6502_addr_null,      cpu65c02_op_phx,     3 }, // 0xda
    { cpu6502_addr_null,      cpu65c02_op_stp,     3 }, // 0xdb
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xdc
    { cpu6502_addr_absx,      cpu6502_op_cmp,      4 }, // 0xdd
    { cpu6502_addr_absx,      cpu6502_op_dec,      7 }, // 0xde
    { cpu6502_addr_zp,        cpu65c02_op_bbs5,    5 }, // 0xdf
    { cpu6502_addr_imm,       cpu6502_op_cpx,      2 }, // 0xe0
    { cpu6502_addr_idxindx,   cpu65c02_op_sbc,     6 }, // 0xe1
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xe2
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xe3
    { cpu6502_addr_zp,        cpu6502_op_cpx,      3 }, // 0xe4
    { cpu6502_addr_zp,        cpu65c02_op_sbc,     3 }, // 0xe5
    { cpu6502_addr_zp,        cpu6502_op_inc,      5 }, // 0xe6
    { cpu6502_addr_zp,        cpu65c02_op_smb6,    5 }, // 0xe7
    { cpu6502_addr_null,      cpu6502_op_inx,      2 }, // 0xe8
    { cpu6502_addr_imm,       cpu65c02_op_sbc,     2 }, // 0xe9
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xea
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xeb
    { cpu6502_addr_abs,       cpu6502_op_cpx,      4 }, // 0xec
    { cpu6502_addr_abs,       cpu65c02_op_sbc,     4 }, // 0xed
    { cpu6502_addr_abs,       cpu6502_op_inc,      6 }, // 0xee
    { cpu6502_addr_zp,        cpu65c02_op_bbs6,    5 }, // 0xef
    { cpu6502_addr_rel,       cpu6502_op_beq,      2 }, // 0xf0
    { cpu6502_addr_indidxy,   cpu65c02_op_sbc,     5 }, // 0xf1
    { cpu65c02_addr_ind_zp,   cpu65c02_op_sbc,     5 }, // 0xf2
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xf3
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xf4
    { cpu6502_addr_zpx,       cpu65c02_op_sbc,     4 }, // 0xf5
    { cpu6502_addr_zpx,       cpu6502_op_inc,      5 }, // 0xf6
    { cpu6502_addr_zp,        cpu65c02_op_smb7,    5 }, // 0xf7
    { cpu6502_addr_null,      cpu6502_op_sed,      2 }, // 0xf8
    { cpu6502_addr_absy,      cpu65c02_op_sbc,     4 }, // 0xf9
    { cpu6502_addr_null,      cpu65c02_op_plx,     4 }, // 0xfa
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xfb
    { cpu6502_addr_null,      cpu6502_op_nop,      2 }, // 0xfc
    { cpu6502_addr_absx,      cpu65c02_op_sbc,     4 }, // 0xfd
    { cpu6502_addr_absx,      cpu6502_op_inc,      7 }, // 0xfe
    { cpu6502_addr_zp,        cpu65c02_op_bbs7,    5 }, // 0xff
};

/* ========================================================================== */

uint16_t cpu65c02_addr_absx(struct cpu65x02* cpu) {
    const uint8_t bal = cpu65x02_get_byte(cpu);
    const uint8_t bah = cpu65x02_get_byte(cpu);
    const uint16_t ba = cpu65x02_word(bal, bah);
    const uint16_t a = ba + cpu->regs.x;

    /*
     * These op-codes take 6 cycles. One extra cycle is needed on page
     * boundary.
     */
    switch(cpu->opcode) {
        case 0x1e:
        case 0x5e:
        case 0x3e:
        case 0x7e:
            cpu6502_pb_correct_cycles(cpu, ba, a);
            break;
    }

    return a;
}

uint16_t cpu65c02_addr_ind_zp(struct cpu65x02* cpu) {
    const uint8_t ial = cpu65x02_get_byte(cpu);
    /* since iah = 0x00, ba = ia */
    const uint8_t adl = cpu->l(ial, cpu->user_data);
    const uint8_t adh = cpu->l(ial + 1, cpu->user_data);
    return cpu65x02_word(adl, adh);
}

uint16_t cpu65c02_addr_abs_indx(struct cpu65x02* cpu) {
    const uint8_t ial = cpu65x02_get_byte(cpu);
    const uint8_t iah = cpu65x02_get_byte(cpu);
    const uint16_t ia = cpu65x02_word(ial, iah);
    const uint16_t ba = ia + cpu->regs.x;
    const uint8_t adl = cpu->l(ba, cpu->user_data);
    const uint8_t adh = cpu->l(ba + 1, cpu->user_data);
    return cpu65x02_word(adl, adh);
}

/* ========================================================================== */

void cpu65c02_op_bra(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.pc = address;
}

void cpu65c02_op_phx(struct cpu65x02* cpu, uint16_t address) {
    (void)address;
    cpu65x02_stack_push(cpu, cpu->regs.x);
}

void cpu65c02_op_phy(struct cpu65x02* cpu, uint16_t address) {
    (void)address;
    cpu65x02_stack_push(cpu, cpu->regs.y);
}

void cpu65c02_op_plx(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.x = cpu65x02_stack_pop(cpu);
    cpu65x02_calc_zero(cpu, cpu->regs.x);
    cpu65x02_calc_sign(cpu, cpu->regs.x);
}

void cpu65c02_op_ply(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.y = cpu65x02_stack_pop(cpu);
    cpu65x02_calc_zero(cpu, cpu->regs.y);
    cpu65x02_calc_sign(cpu, cpu->regs.y);
}

void cpu65c02_op_stz(struct cpu65x02* cpu, uint16_t address) {
    cpu->s(address, 0x00, cpu->user_data);
}

void cpu65c02_op_trb(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    m  &= ~cpu->regs.a;
    cpu->s(address, m, cpu->user_data);
    cpu65x02_calc_zero(cpu, m);
}

void cpu65c02_op_tsb(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    m  |= cpu->regs.a;
    cpu->s(address, m, cpu->user_data);
    cpu65x02_calc_zero(cpu, m);
}

static inline void cpu65c02_op_bbr_common(struct cpu65x02* cpu,
        uint16_t address, uint8_t bit) {
    const uint8_t m = cpu->l(address, cpu->user_data);
    if (!CPU65X02_BIT_TEST(m, bit)) {
        cpu->regs.pc = cpu6502_addr_rel(cpu);
    }
}

static inline void cpu65c02_op_bbs_common(struct cpu65x02* cpu,
        uint16_t address, uint8_t bit) {
    const uint8_t m = cpu->l(address, cpu->user_data);
    if (CPU65X02_BIT_TEST(m, bit)) {
        cpu->regs.pc = cpu6502_addr_rel(cpu);
    }
}

void cpu65c02_op_bbr0(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbr_common(cpu, address, 0);
}

void cpu65c02_op_bbr1(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbr_common(cpu, address, 1);
}

void cpu65c02_op_bbr2(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbr_common(cpu, address, 2);
}

void cpu65c02_op_bbr3(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbr_common(cpu, address, 3);
}

void cpu65c02_op_bbr4(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbr_common(cpu, address, 4);
}

void cpu65c02_op_bbr5(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbr_common(cpu, address, 5);
}

void cpu65c02_op_bbr6(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbr_common(cpu, address, 6);
}

void cpu65c02_op_bbr7(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbr_common(cpu, address, 7);
}

void cpu65c02_op_bbs0(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbs_common(cpu, address, 0);
}

void cpu65c02_op_bbs1(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbs_common(cpu, address, 1);
}

void cpu65c02_op_bbs2(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbs_common(cpu, address, 2);
}

void cpu65c02_op_bbs3(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbs_common(cpu, address, 3);
}

void cpu65c02_op_bbs4(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbs_common(cpu, address, 4);
}

void cpu65c02_op_bbs5(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbs_common(cpu, address, 5);
}

void cpu65c02_op_bbs6(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbs_common(cpu, address, 6);
}

void cpu65c02_op_bbs7(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_bbs_common(cpu, address, 7);
}

void cpu65c02_op_rmb_common(struct cpu65x02* cpu,
        uint16_t address, uint8_t bit) {
    uint8_t m = cpu->l(address, cpu->user_data);
    CPU65X02_BIT_CLEAR(m, bit);
    cpu->s(address, m, cpu->user_data);
}

void cpu65c02_op_smb_common(struct cpu65x02* cpu,
        uint16_t address, uint8_t bit) {
    uint8_t m = cpu->l(address, cpu->user_data);
    CPU65X02_BIT_SET(m, bit);
    cpu->s(address, m, cpu->user_data);
}

void cpu65c02_op_rmb0(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_rmb_common(cpu, address, 0);
}

void cpu65c02_op_rmb1(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_rmb_common(cpu, address, 1);
}

void cpu65c02_op_rmb2(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_rmb_common(cpu, address, 2);
}

void cpu65c02_op_rmb3(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_rmb_common(cpu, address, 3);
}

void cpu65c02_op_rmb4(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_rmb_common(cpu, address, 4);
}

void cpu65c02_op_rmb5(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_rmb_common(cpu, address, 5);
}

void cpu65c02_op_rmb6(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_rmb_common(cpu, address, 6);
}

void cpu65c02_op_rmb7(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_rmb_common(cpu, address, 7);
}

void cpu65c02_op_smb0(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_smb_common(cpu, address, 0);
}

void cpu65c02_op_smb1(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_smb_common(cpu, address, 1);
}

void cpu65c02_op_smb2(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_smb_common(cpu, address, 2);
}

void cpu65c02_op_smb3(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_smb_common(cpu, address, 3);
}

void cpu65c02_op_smb4(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_smb_common(cpu, address, 4);
}

void cpu65c02_op_smb5(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_smb_common(cpu, address, 5);
}

void cpu65c02_op_smb6(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_smb_common(cpu, address, 6);
}

void cpu65c02_op_smb7(struct cpu65x02* cpu, uint16_t address) {
    cpu65c02_op_smb_common(cpu, address, 7);
}

void cpu65c02_op_stp(struct cpu65x02* cpu, uint16_t address) {
    cpu->is_stopped = 1;
}

void cpu65c02_op_wai(struct cpu65x02* cpu, uint16_t address) {
    cpu->is_waiting = 1;
}

void cpu65c02_op_dec(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.a--;
    cpu65x02_calc_zero_acc(cpu);
    cpu65x02_calc_sign_acc(cpu);
}

void cpu65c02_op_inc(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.a++;
    cpu65x02_calc_zero_acc(cpu);
    cpu65x02_calc_sign_acc(cpu);
}

void cpu65c02_op_bit(struct cpu65x02* cpu, uint16_t address) {
    if (0x89 == cpu->opcode) {
        /* immediate addressing handles only zero flag */
        const uint8_t m = cpu->l(address, cpu->user_data);
        const uint8_t tmp = m & cpu->regs.a;
        cpu65x02_calc_zero(cpu, tmp);
    }
    cpu6502_op_bit(cpu, address);
}

/* ========================================================================== */

void cpu65c02_op_brk(struct cpu65x02* cpu, uint16_t address) {
    cpu6502_op_brk(cpu, address);
    CPU65X02_CLEAR_DEC(cpu->regs.status);
}

void cpu65c02_op_adc(struct cpu65x02* cpu, uint16_t address) {
    cpu6502_op_adc(cpu, address);
    cpu65c02_op_adc_sbc_correct_cycles(cpu);
}

void cpu65c02_op_sbc(struct cpu65x02* cpu, uint16_t address) {
    cpu6502_op_sbc(cpu, address);
    cpu65c02_op_adc_sbc_correct_cycles(cpu);
}

/* ========================================================================== */

void cpu65c02_reset(struct cpu65x02 *cpu) {
    cpu6502_reset(cpu);
    CPU65X02_SET_BRK(cpu->regs.status);
    CPU65X02_CLEAR_DEC(cpu->regs.status);
}

void cpu65c02_init(struct cpu65x02* cpu,
        cpu65x02_load_byte_t l,
        cpu65x02_store_byte_t s,
        void* user_data) {
    cpu->s = s;
    cpu->l = l;
    cpu->user_data = user_data;
    cpu->op_handlers = cpu65c02_op_handlers;
    cpu->opcode = 0;
    cpu->cycles = 0;
    cpu->is_stopped = 0;
    cpu->is_waiting = 0;

	// clear the registers
	memset(&cpu->regs, 0x00, sizeof(cpu->regs));
}

/* ========================================================================== */
