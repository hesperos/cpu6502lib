#include "include/cpu65x02/cpu65x02_api.h"
#include "include/cpu65x02/cpu65x02_constants.h"
#include "include/cpu65x02/cpu65x02_debug.h"
#include "include/cpu65x02/cpu65x02_status.h"
#include "include/cpu65x02/cpu65x02_util.h"

#include <string.h>

/* ========================================================================== */

void cpu65x02_step(struct cpu65x02 *cpu) {
    /* check if cpu is halted or sleeping */
    if (cpu->is_stopped || cpu->is_waiting) {
        return;
    }

    /* fetch new op-code */
	const uint8_t opcode = cpu65x02_get_byte(cpu);
    cpu->opcode = opcode;

    /* generate operand's address */
    const struct cpu65x02_op_handler* oh = &cpu->op_handlers[opcode];
    const uint16_t address = oh->addresser(cpu);

    /* print some context in debugging mode */
    cpu65x02_disasm_opcode(cpu, address);

    /* execute instruction */
    oh->executor(cpu, address);
    cpu65x02_tick_n(cpu, oh->base_cycles);
}

void cpu65x02_irq(struct cpu65x02* cpu) {
    /* clear waiting flag */
    cpu->is_waiting = 0;

    if (CPU65X02_TEST_INT(cpu->regs.status)) {
        /* interrupts are disabled, do nothing */
        return;
    }

    cpu65x02_stack_push_pc(cpu);

    CPU65X02_CLEAR_BRK(cpu->regs.status);
    cpu65x02_stack_push(cpu, cpu->regs.status);

    CPU65X02_SET_INT(cpu->regs.status);
    cpu65x02_load_vector(cpu, CPU65X02_IRQ_VECTOR);
}

void cpu65x02_nmi(struct cpu65x02* cpu) {
    /* clear waiting flag */
    cpu->is_waiting = 0;

    cpu65x02_stack_push_pc(cpu);

    CPU65X02_CLEAR_BRK(cpu->regs.status);
    cpu65x02_stack_push(cpu, cpu->regs.status);

    CPU65X02_SET_INT(cpu->regs.status);
    cpu65x02_load_vector(cpu, CPU65X02_NMI_VECTOR);
}

/* ========================================================================== */
