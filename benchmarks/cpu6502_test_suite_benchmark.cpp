#include <benchmark/benchmark.h>
#include <cpu65x02/cpu65x02.h>
#include <benchmarks/config.h>

#include <array>
#include <fstream>

// =============================================================================

namespace {
    class TestSystem {
    public:

        TestSystem(const std::string& rom_path,
                uint16_t offset,
                bool is_cmos) :
            offset(offset),
            is_cmos(is_cmos)
        {
            load_rom(rom_path);

            if (is_cmos) {
                cpu65c02_init(&cpu, load, store, NULL);
            }
            else {
                cpu6502_init(&cpu, load, store, NULL);
            }

            reset();
        }

        static void store(uint16_t address, uint8_t data, void*) {
            memory[address] = data;
        }

        static uint8_t load(uint16_t address, void*) {
            return memory[address];
        }

        void load_rom(const std::string& rom_path) {
            std::ifstream rom_file(rom_path, std::ios::binary);

            if (!rom_file.is_open()) {
                throw std::runtime_error("Unable to open rom_file");
            }

            size_t position = 0x00;
            while (position < memory_size && rom_file.good())
            {
                memory[position++] = rom_file.get();
            }
        }

        void reset() {
            if (is_cmos) {
                cpu65c02_reset(&cpu);
            }
            else {
                cpu6502_reset(&cpu);
            }
        }

        void run(uint32_t cycles) {
            cpu.regs.pc = offset;
            for (;;) {
                cpu65x02_step(&cpu);
                if (cpu.cycles > cycles) {
                    break;
                }
            }
        }

    private:
        static constexpr size_t memory_size = 64 * 1024;
        static std::array<uint8_t, memory_size> memory;

        const size_t offset;
        const bool is_cmos;

        struct cpu65x02 cpu;
    };

    // memory definition
    std::array<uint8_t, TestSystem::memory_size> TestSystem::memory;
} // namespace

// =============================================================================

static void BM_6502_test_suite_cycles(benchmark::State& state) {
    const std::string rom_path =
        std::string(src_3rdparty_path) +
        "/6502_65C02_functional_tests/bin_files/"
        "6502_functional_test.bin";

    const uint16_t load_offset = 0x400;
    TestSystem ts(rom_path, load_offset, false);

    while (state.KeepRunning()) {
        ts.reset();
        ts.run(state.range(0));
    }
}
// benchmark execution of given number of cycles
BENCHMARK(BM_6502_test_suite_cycles)
    ->Unit(benchmark::kMillisecond)
    ->Range(100000, 100000000);

// =============================================================================

static void BM_65C02_test_suite_cycles(benchmark::State& state) {
    const std::string rom_path =
        std::string(src_3rdparty_path) +
        "/6502_65C02_functional_tests/bin_files/"
        "6502_functional_test.bin";

    const uint16_t load_offset = 0x400;
    TestSystem ts(rom_path, load_offset, true);

    while (state.KeepRunning()) {
        ts.reset();
        ts.run(state.range(0));
    }
}
// benchmark execution of given number of cycles
BENCHMARK(BM_65C02_test_suite_cycles)
    ->Unit(benchmark::kMillisecond)
    ->Range(100000, 100000000);

// =============================================================================

// google benchmark entry point
BENCHMARK_MAIN();
