#include "include/cpu65x02/cpu6502_api.h"
#include "include/cpu65x02/cpu65x02_bcd.h"
#include "include/cpu65x02/cpu65x02_constants.h"
#include "include/cpu65x02/cpu65x02_debug.h"
#include "include/cpu65x02/cpu65x02_status.h"
#include "include/cpu65x02/cpu65x02_util.h"

#include "include/cpu65x02/private/cpu6502_api.h"

#include <string.h>

/* ========================================================================== */

static struct cpu65x02_op_handler cpu6502_op_handlers[CPU65X02_MAX_OPCODES] = {
    { cpu6502_addr_null,    cpu6502_op_brk,     7 }, // 0x00
    { cpu6502_addr_idxindx, cpu6502_op_ora,     6 }, // 0x01
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x02
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x03
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x04
    { cpu6502_addr_zp,      cpu6502_op_ora,     3 }, // 0x05
    { cpu6502_addr_zp,      cpu6502_op_asl,     5 }, // 0x06
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x07
    { cpu6502_addr_null,    cpu6502_op_php,     3 }, // 0x08
    { cpu6502_addr_imm,     cpu6502_op_ora,     2 }, // 0x09
    { cpu6502_addr_null,    cpu6502_op_asl_acc, 2 }, // 0x0a
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x0b
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x0c
    { cpu6502_addr_abs,     cpu6502_op_ora,     4 }, // 0x0d
    { cpu6502_addr_abs,     cpu6502_op_asl,     6 }, // 0x0e
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x0f
    { cpu6502_addr_rel,     cpu6502_op_bpl,     2 }, // 0x10
    { cpu6502_addr_indidxy, cpu6502_op_ora,     5 }, // 0x11
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x12
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x13
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x14
    { cpu6502_addr_zpx,     cpu6502_op_ora,     4 }, // 0x15
    { cpu6502_addr_zpx,     cpu6502_op_asl,     6 }, // 0x16
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x17
    { cpu6502_addr_null,    cpu6502_op_clc,     2 }, // 0x18
    { cpu6502_addr_absy,    cpu6502_op_ora,     4 }, // 0x19
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x1a
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x1b
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x1c
    { cpu6502_addr_absx,    cpu6502_op_ora,     4 }, // 0x1d
    { cpu6502_addr_absx,    cpu6502_op_asl,     7 }, // 0x1e
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x1f
    { cpu6502_addr_abs,     cpu6502_op_jsr,     6 }, // 0x20
    { cpu6502_addr_idxindx, cpu6502_op_and,     6 }, // 0x21
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x22
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x23
    { cpu6502_addr_zp,      cpu6502_op_bit,     3 }, // 0x24
    { cpu6502_addr_zp,      cpu6502_op_and,     3 }, // 0x25
    { cpu6502_addr_zp,      cpu6502_op_rol,     5 }, // 0x26
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x27
    { cpu6502_addr_null,    cpu6502_op_plp,     4 }, // 0x28
    { cpu6502_addr_imm,     cpu6502_op_and,     2 }, // 0x29
    { cpu6502_addr_null,    cpu6502_op_rol_acc, 2 }, // 0x2a
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x2b
    { cpu6502_addr_abs,     cpu6502_op_bit,     4 }, // 0x2c
    { cpu6502_addr_abs,     cpu6502_op_and,     4 }, // 0x2d
    { cpu6502_addr_abs,     cpu6502_op_rol,     6 }, // 0x2e
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x2f
    { cpu6502_addr_rel,     cpu6502_op_bmi,     2 }, // 0x30
    { cpu6502_addr_indidxy, cpu6502_op_and,     5 }, // 0x31
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x32
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x33
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x34
    { cpu6502_addr_zpx,     cpu6502_op_and,     4 }, // 0x35
    { cpu6502_addr_zpx,     cpu6502_op_rol,     6 }, // 0x36
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x37
    { cpu6502_addr_null,    cpu6502_op_sec,     2 }, // 0x38
    { cpu6502_addr_absy,    cpu6502_op_and,     4 }, // 0x39
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x3a
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x3b
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x3c
    { cpu6502_addr_absx,    cpu6502_op_and,     4 }, // 0x3d
    { cpu6502_addr_absx,    cpu6502_op_rol,     7 }, // 0x3e
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x3f
    { cpu6502_addr_null,    cpu6502_op_rti,     6 }, // 0x40
    { cpu6502_addr_idxindx, cpu6502_op_eor,     6 }, // 0x41
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x42
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x43
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x44
    { cpu6502_addr_zp,      cpu6502_op_eor,     3 }, // 0x45
    { cpu6502_addr_zp,      cpu6502_op_lsr,     5 }, // 0x46
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x47
    { cpu6502_addr_null,    cpu6502_op_pha,     3 }, // 0x48
    { cpu6502_addr_imm,     cpu6502_op_eor,     2 }, // 0x49
    { cpu6502_addr_null,    cpu6502_op_lsr_acc, 2 }, // 0x4a
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x4b
    { cpu6502_addr_abs,     cpu6502_op_jmp,     3 }, // 0x4c
    { cpu6502_addr_abs,     cpu6502_op_eor,     4 }, // 0x4d
    { cpu6502_addr_abs,     cpu6502_op_lsr,     6 }, // 0x4e
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x4f
    { cpu6502_addr_rel,     cpu6502_op_bvc,     2 }, // 0x50
    { cpu6502_addr_indidxy, cpu6502_op_eor,     5 }, // 0x51
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x52
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x53
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x54
    { cpu6502_addr_zpx,     cpu6502_op_eor,     4 }, // 0x55
    { cpu6502_addr_zpx,     cpu6502_op_lsr,     6 }, // 0x56
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x57
    { cpu6502_addr_null,    cpu6502_op_cli,     2 }, // 0x58
    { cpu6502_addr_absy,    cpu6502_op_eor,     4 }, // 0x59
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x5a
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x5b
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x5c
    { cpu6502_addr_absx,    cpu6502_op_eor,     4 }, // 0x5d
    { cpu6502_addr_absx,    cpu6502_op_lsr,     7 }, // 0x5e
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x5f
    { cpu6502_addr_null,    cpu6502_op_rts,     6 }, // 0x60
    { cpu6502_addr_idxindx, cpu6502_op_adc,     6 }, // 0x61
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x62
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x63
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x64
    { cpu6502_addr_zp,      cpu6502_op_adc,     3 }, // 0x65
    { cpu6502_addr_zp,      cpu6502_op_ror,     5 }, // 0x66
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x67
    { cpu6502_addr_null,    cpu6502_op_pla,     4 }, // 0x68
    { cpu6502_addr_imm,     cpu6502_op_adc,     2 }, // 0x69
    { cpu6502_addr_null,    cpu6502_op_ror_acc, 2 }, // 0x6a
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x6b
    { cpu6502_addr_ind,     cpu6502_op_jmp,     5 }, // 0x6c
    { cpu6502_addr_abs,     cpu6502_op_adc,     4 }, // 0x6d
    { cpu6502_addr_abs,     cpu6502_op_ror,     6 }, // 0x6e
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x6f
    { cpu6502_addr_rel,     cpu6502_op_bvs,     2 }, // 0x70
    { cpu6502_addr_indidxy, cpu6502_op_adc,     5 }, // 0x71
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x72
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x73
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x74
    { cpu6502_addr_zpx,     cpu6502_op_adc,     4 }, // 0x75
    { cpu6502_addr_zpx,     cpu6502_op_ror,     6 }, // 0x76
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x77
    { cpu6502_addr_null,    cpu6502_op_sei,     2 }, // 0x78
    { cpu6502_addr_absy,    cpu6502_op_adc,     4 }, // 0x79
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x7a
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x7b
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x7c
    { cpu6502_addr_absx,    cpu6502_op_adc,     4 }, // 0x7d
    { cpu6502_addr_absx,    cpu6502_op_ror,     7 }, // 0x7e
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x7f
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x80
    { cpu6502_addr_idxindx, cpu6502_op_sta,     6 }, // 0x81
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x82
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x83
    { cpu6502_addr_zp,      cpu6502_op_sty,     3 }, // 0x84
    { cpu6502_addr_zp,      cpu6502_op_sta,     3 }, // 0x85
    { cpu6502_addr_zp,      cpu6502_op_stx,     3 }, // 0x86
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x87
    { cpu6502_addr_null,    cpu6502_op_dey,     2 }, // 0x88
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x89
    { cpu6502_addr_null,    cpu6502_op_txa,     2 }, // 0x8a
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x8b
    { cpu6502_addr_abs,     cpu6502_op_sty,     4 }, // 0x8c
    { cpu6502_addr_abs,     cpu6502_op_sta,     4 }, // 0x8d
    { cpu6502_addr_abs,     cpu6502_op_stx,     4 }, // 0x8e
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x8f
    { cpu6502_addr_rel,     cpu6502_op_bcc,     2 }, // 0x90
    { cpu6502_addr_indidxy, cpu6502_op_sta,     6 }, // 0x91
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x92
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x93
    { cpu6502_addr_zpx,     cpu6502_op_sty,     4 }, // 0x94
    { cpu6502_addr_zpx,     cpu6502_op_sta,     4 }, // 0x95
    { cpu6502_addr_zpy,     cpu6502_op_stx,     4 }, // 0x96
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x97
    { cpu6502_addr_null,    cpu6502_op_tya,     2 }, // 0x98
    { cpu6502_addr_absy,    cpu6502_op_sta,     5 }, // 0x99
    { cpu6502_addr_null,    cpu6502_op_txs,     2 }, // 0x9a
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x9b
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x9c
    { cpu6502_addr_absx,    cpu6502_op_sta,     5 }, // 0x9d
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x9e
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0x9f
    { cpu6502_addr_imm,     cpu6502_op_ldy,     2 }, // 0xa0
    { cpu6502_addr_idxindx, cpu6502_op_lda,     6 }, // 0xa1
    { cpu6502_addr_imm,     cpu6502_op_ldx,     2 }, // 0xa2
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xa3
    { cpu6502_addr_zp,      cpu6502_op_ldy,     3 }, // 0xa4
    { cpu6502_addr_zp,      cpu6502_op_lda,     3 }, // 0xa5
    { cpu6502_addr_zp,      cpu6502_op_ldx,     3 }, // 0xa6
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xa7
    { cpu6502_addr_null,    cpu6502_op_tay,     2 }, // 0xa8
    { cpu6502_addr_imm,     cpu6502_op_lda,     2 }, // 0xa9
    { cpu6502_addr_null,    cpu6502_op_tax,     2 }, // 0xaa
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xab
    { cpu6502_addr_abs,     cpu6502_op_ldy,     4 }, // 0xac
    { cpu6502_addr_abs,     cpu6502_op_lda,     4 }, // 0xad
    { cpu6502_addr_abs,     cpu6502_op_ldx,     4 }, // 0xae
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xaf
    { cpu6502_addr_rel,     cpu6502_op_bcs,     2 }, // 0xb0
    { cpu6502_addr_indidxy, cpu6502_op_lda,     5 }, // 0xb1
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xb2
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xb3
    { cpu6502_addr_zpx,     cpu6502_op_ldy,     4 }, // 0xb4
    { cpu6502_addr_zpx,     cpu6502_op_lda,     4 }, // 0xb5
    { cpu6502_addr_zpy,     cpu6502_op_ldx,     4 }, // 0xb6
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xb7
    { cpu6502_addr_null,    cpu6502_op_clv,     2 }, // 0xb8
    { cpu6502_addr_absy,    cpu6502_op_lda,     4 }, // 0xb9
    { cpu6502_addr_null,    cpu6502_op_tsx,     2 }, // 0xba
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xbb
    { cpu6502_addr_absx,    cpu6502_op_ldy,     4 }, // 0xbc
    { cpu6502_addr_absx,    cpu6502_op_lda,     4 }, // 0xbd
    { cpu6502_addr_absy,    cpu6502_op_ldx,     4 }, // 0xbe
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xbf
    { cpu6502_addr_imm,     cpu6502_op_cpy,     2 }, // 0xc0
    { cpu6502_addr_idxindx, cpu6502_op_cmp,     6 }, // 0xc1
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xc2
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xc3
    { cpu6502_addr_zp,      cpu6502_op_cpy,     3 }, // 0xc4
    { cpu6502_addr_zp,      cpu6502_op_cmp,     3 }, // 0xc5
    { cpu6502_addr_zp,      cpu6502_op_dec,     5 }, // 0xc6
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xc7
    { cpu6502_addr_null,    cpu6502_op_iny,     2 }, // 0xc8
    { cpu6502_addr_imm,     cpu6502_op_cmp,     2 }, // 0xc9
    { cpu6502_addr_null,    cpu6502_op_dex,     2 }, // 0xca
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xcb
    { cpu6502_addr_abs,     cpu6502_op_cpy,     4 }, // 0xcc
    { cpu6502_addr_abs,     cpu6502_op_cmp,     4 }, // 0xcd
    { cpu6502_addr_abs,     cpu6502_op_dec,     6 }, // 0xce
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xcf
    { cpu6502_addr_rel,     cpu6502_op_bne,     2 }, // 0xd0
    { cpu6502_addr_indidxy, cpu6502_op_cmp,     5 }, // 0xd1
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xd2
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xd3
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xd4
    { cpu6502_addr_zpx,     cpu6502_op_cmp,     4 }, // 0xd5
    { cpu6502_addr_zpx,     cpu6502_op_dec,     6 }, // 0xd6
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xd7
    { cpu6502_addr_null,    cpu6502_op_cld,     2 }, // 0xd8
    { cpu6502_addr_absy,    cpu6502_op_cmp,     4 }, // 0xd9
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xda
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xdb
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xdc
    { cpu6502_addr_absx,    cpu6502_op_cmp,     4 }, // 0xdd
    { cpu6502_addr_absx,    cpu6502_op_dec,     7 }, // 0xde
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xdf
    { cpu6502_addr_imm,     cpu6502_op_cpx,     2 }, // 0xe0
    { cpu6502_addr_idxindx, cpu6502_op_sbc,     6 }, // 0xe1
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xe2
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xe3
    { cpu6502_addr_zp,      cpu6502_op_cpx,     3 }, // 0xe4
    { cpu6502_addr_zp,      cpu6502_op_sbc,     3 }, // 0xe5
    { cpu6502_addr_zp,      cpu6502_op_inc,     5 }, // 0xe6
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xe7
    { cpu6502_addr_null,    cpu6502_op_inx,     2 }, // 0xe8
    { cpu6502_addr_imm,     cpu6502_op_sbc,     2 }, // 0xe9
    { cpu6502_addr_null,    cpu6502_op_nop,     2 }, // 0xea
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xeb
    { cpu6502_addr_abs,     cpu6502_op_cpx,     4 }, // 0xec
    { cpu6502_addr_abs,     cpu6502_op_sbc,     4 }, // 0xed
    { cpu6502_addr_abs,     cpu6502_op_inc,     6 }, // 0xee
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xef
    { cpu6502_addr_rel,     cpu6502_op_beq,     2 }, // 0xf0
    { cpu6502_addr_indidxy, cpu6502_op_sbc,     5 }, // 0xf1
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xf2
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xf3
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xf4
    { cpu6502_addr_zpx,     cpu6502_op_sbc,     4 }, // 0xf5
    { cpu6502_addr_zpx,     cpu6502_op_inc,     5 }, // 0xf6
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xf7
    { cpu6502_addr_null,    cpu6502_op_sed,     2 }, // 0xf8
    { cpu6502_addr_absy,    cpu6502_op_sbc,     4 }, // 0xf9
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xfa
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xfb
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xfc
    { cpu6502_addr_absx,    cpu6502_op_sbc,     4 }, // 0xfd
    { cpu6502_addr_absx,    cpu6502_op_inc,     7 }, // 0xfe
    { cpu6502_addr_null,    cpu6502_op_illegal, 0 }, // 0xff
};

/* ========================================================================== */

static void cpu6502_addr_abs_correct_cycles(struct cpu65x02* cpu,
        uint16_t ba, uint16_t a) {
    if (cpu6502_op_handlers[cpu->opcode].base_cycles != 4) {
        return;
    }
    cpu6502_pb_correct_cycles(cpu, ba, a);
}

static void cpu6502_addr_ind_correct_cycles(struct cpu65x02* cpu,
        uint16_t ba, uint16_t a) {
    if (cpu6502_op_handlers[cpu->opcode].base_cycles != 5) {
        return;
    }
    cpu6502_pb_correct_cycles(cpu, ba, a);
}

static void cpu6502_addr_rel_correct_cycles(struct cpu65x02* cpu,
        uint16_t a) {
    cpu6502_pb_correct_cycles(cpu, cpu->regs.pc, a);
}

/* ========================================================================== */

uint16_t cpu6502_addr_null(struct cpu65x02* cpu) {
    (void)cpu;
    return 0x0000;
}

uint16_t cpu6502_addr_imm(struct cpu65x02* cpu) {
    return cpu->regs.pc++;
}

uint16_t cpu6502_addr_rel(struct cpu65x02* cpu) {
    const int8_t offset = cpu65x02_get_byte(cpu);
    const uint16_t a = cpu->regs.pc + offset;
    cpu6502_addr_rel_correct_cycles(cpu, a);
    return a;
}

uint16_t cpu6502_addr_zp(struct cpu65x02* cpu) {
    const uint8_t adl = cpu65x02_get_byte(cpu);
    return adl;
}

uint16_t cpu6502_addr_zpx(struct cpu65x02* cpu) {
    const uint8_t bal = cpu65x02_get_byte(cpu);
    return (bal + cpu->regs.x) & 0xff;
}

uint16_t cpu6502_addr_zpy(struct cpu65x02* cpu) {
    const uint8_t bal = cpu65x02_get_byte(cpu);
    return (bal + cpu->regs.y) & 0xff;
}

uint16_t cpu6502_addr_abs(struct cpu65x02* cpu) {
    const uint8_t adl = cpu65x02_get_byte(cpu);
    return cpu65x02_word(adl, cpu65x02_get_byte(cpu));
}

uint16_t cpu6502_addr_absx(struct cpu65x02* cpu) {
    const uint8_t bal = cpu65x02_get_byte(cpu);
    const uint8_t bah = cpu65x02_get_byte(cpu);
    const uint16_t ba = cpu65x02_word(bal, bah);
    const uint16_t a = ba + cpu->regs.x;
    cpu6502_addr_abs_correct_cycles(cpu, ba, a);
    return a;
}

uint16_t cpu6502_addr_absy(struct cpu65x02* cpu) {
    const uint8_t bal = cpu65x02_get_byte(cpu);
    const uint16_t bah = cpu65x02_get_byte(cpu);
    const uint16_t ba = cpu65x02_word(bal, bah);
    const uint16_t a = ba + cpu->regs.y;
    cpu6502_addr_abs_correct_cycles(cpu, ba, a);
    return a;
}

uint16_t cpu6502_addr_ind(struct cpu65x02* cpu) {
    const uint8_t ial = cpu65x02_get_byte(cpu);
    const uint8_t iah = cpu65x02_get_byte(cpu);
    const uint16_t ba = cpu65x02_word(ial, iah);
    const uint8_t adl = cpu->l(ba, cpu->user_data);
    const uint8_t adh = cpu->l(ba + 1, cpu->user_data);
    return cpu65x02_word(adl, adh);
}

uint16_t cpu6502_addr_idxindx(struct cpu65x02* cpu) {
    const uint8_t bal = cpu65x02_get_byte(cpu) + cpu->regs.x;
    const uint8_t adl = cpu->l(bal, cpu->user_data);
    const uint8_t adh = cpu->l(bal + 1, cpu->user_data);
    return cpu65x02_word(adl, adh);
}

uint16_t cpu6502_addr_indidxy(struct cpu65x02* cpu) {
    const uint8_t ial = cpu65x02_get_byte(cpu);
    const uint8_t bal = cpu->l(ial, cpu->user_data);
    const uint8_t bah = cpu->l(ial + 1, cpu->user_data);
    const uint16_t ba = cpu65x02_word(bal, bah);
    const uint16_t a = ba + cpu->regs.y;
    cpu6502_addr_ind_correct_cycles(cpu, ba, a);
    return a;
}

/* ========================================================================== */

void cpu6502_op_and(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    cpu->regs.a &= m;
    cpu65x02_calc_sign_acc(cpu);
    cpu65x02_calc_zero_acc(cpu);
}

void cpu6502_op_asl_common(struct cpu65x02* cpu, uint16_t* value) {
    *value <<= 1;
    cpu65x02_calc_carry(cpu, *value);
    *value &= 0xff;
    cpu65x02_calc_sign(cpu, *value);
    cpu65x02_calc_zero(cpu, *value);
}

void cpu6502_op_asl(struct cpu65x02* cpu, uint16_t address) {
    uint16_t m = cpu->l(address, cpu->user_data);
    cpu6502_op_asl_common(cpu, &m);
    cpu->s(address, m, cpu->user_data);
}

void cpu6502_op_asl_acc(struct cpu65x02* cpu, uint16_t address) {
    uint16_t m = cpu->regs.a;
    cpu6502_op_asl_common(cpu, &m);
    cpu->regs.a = m;
}

void cpu6502_op_lsr_common(struct cpu65x02* cpu, uint16_t* value) {
    if (*value & 0x01) {
        CPU65X02_SET_CARRY(cpu->regs.status);
    }
    else {
        CPU65X02_CLEAR_CARRY(cpu->regs.status);
    }

    *value >>= 1;
    *value &= 0xff;

	CPU65X02_CLEAR_SIGN(cpu->regs.status);
    cpu65x02_calc_zero(cpu, *value);
}

void cpu6502_op_lsr(struct cpu65x02* cpu, uint16_t address) {
    uint16_t m = cpu->l(address, cpu->user_data);
    cpu6502_op_lsr_common(cpu, &m);
    cpu->s(address, m, cpu->user_data);
}

void cpu6502_op_lsr_acc(struct cpu65x02* cpu, uint16_t address) {
    uint16_t m = cpu->regs.a;
    cpu6502_op_lsr_common(cpu, &m);
    cpu->regs.a = m;
}

void cpu6502_op_rol_common(struct cpu65x02* cpu, uint16_t* value) {
    uint8_t newCarry = *value & 0x80;
    *value <<= 1;
    *value |= CPU65X02_GET_CARRY(cpu->regs.status);

    cpu65x02_calc_sign(cpu, *value);
    cpu65x02_calc_zero(cpu, *value);

    if (newCarry) {
        CPU65X02_SET_CARRY(cpu->regs.status);
    }
    else {
        CPU65X02_CLEAR_CARRY(cpu->regs.status);
    }
}

void cpu6502_op_rol(struct cpu65x02* cpu, uint16_t address) {
    uint16_t m = cpu->l(address, cpu->user_data);
    cpu6502_op_rol_common(cpu, &m);
    cpu->s(address, m, cpu->user_data);
}

void cpu6502_op_rol_acc(struct cpu65x02* cpu, uint16_t address) {
    uint16_t m = cpu->regs.a;
    cpu6502_op_rol_common(cpu, &m);
    cpu->regs.a = m;
}

void cpu6502_op_ror_common(struct cpu65x02* cpu, uint16_t* value) {
    uint8_t newCarry = *value & 0x01;
    *value >>= 1;
    *value |= (CPU65X02_GET_CARRY(cpu->regs.status) << 7);
    cpu65x02_calc_sign(cpu, *value);
    cpu65x02_calc_zero(cpu, *value);
    if (newCarry) {
        CPU65X02_SET_CARRY(cpu->regs.status);
    }
    else {
        CPU65X02_CLEAR_CARRY(cpu->regs.status);
    }
}

void cpu6502_op_ror(struct cpu65x02* cpu, uint16_t address) {
    uint16_t m = cpu->l(address, cpu->user_data);
    cpu6502_op_ror_common(cpu, &m);
    cpu->s(address, m, cpu->user_data);
}

void cpu6502_op_ror_acc(struct cpu65x02* cpu, uint16_t address) {
    uint16_t m = cpu->regs.a;
    cpu6502_op_ror_common(cpu, &m);
    cpu->regs.a = m;
}

void cpu6502_op_adc(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    uint16_t tmp = cpu->regs.a + m +
        CPU65X02_GET_CARRY(cpu->regs.status);

    cpu6502_adc_decimal_adjust(cpu, m, &tmp);

    // must be done before original value in a is overwritten
    cpu65x02_calc_ovr(cpu, cpu->regs.a, m, tmp);
    cpu->regs.a = (uint8_t)(tmp & 0xff);

    cpu65x02_calc_sign_acc(cpu);
    cpu65x02_calc_carry(cpu, tmp);
    cpu65x02_calc_zero_acc(cpu);
}

void cpu6502_op_brk(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.pc++;
    cpu65x02_stack_push_pc(cpu);
    CPU65X02_SET_BRK(cpu->regs.status);
    cpu65x02_stack_push(cpu, cpu->regs.status);
    CPU65X02_SET_INT(cpu->regs.status);
    cpu65x02_load_vector(cpu, CPU65X02_IRQ_VECTOR);
}

void cpu6502_op_bcc(struct cpu65x02* cpu, uint16_t address) {
    if (!CPU65X02_TEST_CARRY(cpu->regs.status)) {
        cpu65x02_tick(cpu);
        cpu->regs.pc = address;
    }
}

void cpu6502_op_bcs(struct cpu65x02* cpu, uint16_t address) {
    if (CPU65X02_TEST_CARRY(cpu->regs.status)) {
        cpu65x02_tick(cpu);
        cpu->regs.pc = address;
    }
}

void cpu6502_op_beq(struct cpu65x02* cpu, uint16_t address) {
    if (CPU65X02_TEST_ZERO(cpu->regs.status)) {
        cpu65x02_tick(cpu);
        cpu->regs.pc = address;
    }
}

void cpu6502_op_bit(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    uint8_t tmp = m & cpu->regs.a;

    cpu65x02_calc_zero(cpu, tmp);
    cpu65x02_calc_sign(cpu, m);

    if (m & 0x40) {
        CPU65X02_SET_OVR(cpu->regs.status);
    }
    else {
        CPU65X02_CLEAR_OVR(cpu->regs.status);
    }
}

void cpu6502_op_bmi(struct cpu65x02* cpu, uint16_t address) {
    if (CPU65X02_TEST_SIGN(cpu->regs.status)) {
        cpu65x02_tick(cpu);
        cpu->regs.pc = address;
    }
}

void cpu6502_op_bne(struct cpu65x02* cpu, uint16_t address) {
    if (!CPU65X02_TEST_ZERO(cpu->regs.status)) {
        cpu65x02_tick(cpu);
        cpu->regs.pc = address;
    }
}

void cpu6502_op_bpl(struct cpu65x02* cpu, uint16_t address) {
    if (!CPU65X02_TEST_SIGN(cpu->regs.status)) {
        cpu65x02_tick(cpu);
        cpu->regs.pc = address;
    }
}

void cpu6502_op_bvc(struct cpu65x02* cpu, uint16_t address) {
    if (!CPU65X02_TEST_OVR(cpu->regs.status)) {
        cpu65x02_tick(cpu);
        cpu->regs.pc = address;
    }
}

void cpu6502_op_bvs(struct cpu65x02* cpu, uint16_t address) {
    if (CPU65X02_TEST_OVR(cpu->regs.status)) {
        cpu65x02_tick(cpu);
        cpu->regs.pc = address;
    }
}

void cpu6502_op_clc(struct cpu65x02* cpu, uint16_t address) {
    CPU65X02_CLEAR_CARRY(cpu->regs.status);
}

void cpu6502_op_cld(struct cpu65x02* cpu, uint16_t address) {
    CPU65X02_CLEAR_DEC(cpu->regs.status);
}

void cpu6502_op_cli(struct cpu65x02* cpu, uint16_t address) {
    CPU65X02_CLEAR_INT(cpu->regs.status);
}

void cpu6502_op_clv(struct cpu65x02* cpu, uint16_t address) {
    CPU65X02_CLEAR_OVR(cpu->regs.status);
}

void cpu6502_op_cmp(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    uint16_t tmp = cpu->regs.a - m;
    cpu65x02_calc_zero(cpu, tmp);
    cpu65x02_calc_sign(cpu, tmp);
    cpu65x02_calc_borrow(cpu, tmp);
}

void cpu6502_op_cpx(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    uint16_t tmp = cpu->regs.x - m;
    cpu65x02_calc_zero(cpu, tmp);
    cpu65x02_calc_sign(cpu, tmp);
    cpu65x02_calc_borrow(cpu, tmp);
}

void cpu6502_op_cpy(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    uint16_t tmp = cpu->regs.y - m;
    cpu65x02_calc_zero(cpu, tmp);
    cpu65x02_calc_sign(cpu, tmp);
    cpu65x02_calc_borrow(cpu, tmp);
}

void cpu6502_op_dec(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    uint8_t tmp = m - 1;
    cpu->s(address, tmp, cpu->user_data);
    cpu65x02_calc_zero(cpu, tmp);
    cpu65x02_calc_sign(cpu, tmp);
}

void cpu6502_op_dex(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.x--;
    cpu65x02_calc_zero(cpu, cpu->regs.x);
    cpu65x02_calc_sign(cpu, cpu->regs.x);
}

void cpu6502_op_dey(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.y--;
    cpu65x02_calc_zero(cpu, cpu->regs.y);
    cpu65x02_calc_sign(cpu, cpu->regs.y);
}

void cpu6502_op_eor(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    cpu->regs.a ^= m;
    cpu65x02_calc_zero_acc(cpu);
    cpu65x02_calc_sign_acc(cpu);
}

void cpu6502_op_inc(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    uint8_t tmp = m + 1;
    cpu->s(address, tmp, cpu->user_data);
    cpu65x02_calc_zero(cpu, tmp);
    cpu65x02_calc_sign(cpu, tmp);
}

void cpu6502_op_inx(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.x++;
    cpu65x02_calc_zero(cpu, cpu->regs.x);
    cpu65x02_calc_sign(cpu, cpu->regs.x);
}

void cpu6502_op_iny(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.y++;
    cpu65x02_calc_zero(cpu, cpu->regs.y);
    cpu65x02_calc_sign(cpu, cpu->regs.y);
}

void cpu6502_op_jmp(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.pc = address;
}

void cpu6502_op_jsr(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.pc--;
    cpu65x02_stack_push_pc(cpu);
    cpu->regs.pc = address;
}

void cpu6502_op_lda(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.a = cpu->l(address, cpu->user_data);
    cpu65x02_calc_zero_acc(cpu);
    cpu65x02_calc_sign_acc(cpu);
}

void cpu6502_op_ldx(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.x = cpu->l(address, cpu->user_data);
    cpu65x02_calc_zero(cpu, cpu->regs.x);
    cpu65x02_calc_sign(cpu, cpu->regs.x);
}

void cpu6502_op_ldy(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.y = cpu->l(address, cpu->user_data);
    cpu65x02_calc_zero(cpu, cpu->regs.y);
    cpu65x02_calc_sign(cpu, cpu->regs.y);
}

void cpu6502_op_nop(struct cpu65x02* cpu, uint16_t address) {
}

void cpu6502_op_ora(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    cpu->regs.a |= m;
    cpu65x02_calc_zero_acc(cpu);
    cpu65x02_calc_sign_acc(cpu);
}

void cpu6502_op_pha(struct cpu65x02* cpu, uint16_t address) {
    cpu65x02_stack_push(cpu, cpu->regs.a);
}

void cpu6502_op_php(struct cpu65x02* cpu, uint16_t address) {
    uint8_t tmp = cpu->regs.status;
    // apparently BRK must be set during PHP, it should be cleared only by IRQ
    CPU65X02_SET_BRK(tmp);
    cpu65x02_stack_push(cpu, tmp);
}

void cpu6502_op_pla(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.a = cpu65x02_stack_pop(cpu);
    cpu65x02_calc_zero_acc(cpu);
    cpu65x02_calc_sign_acc(cpu);
}

void cpu6502_op_plp(struct cpu65x02* cpu, uint16_t address) {
    uint8_t orig = cpu->regs.status;
    cpu->regs.status = cpu65x02_stack_pop(cpu);

    CPU65X02_SET_UNUSED(cpu->regs.status);

    // restore original value as it shouldn't be restored from memory
    if (CPU65X02_TEST_BRK(orig)) {
        CPU65X02_SET_BRK(cpu->regs.status);
    }
    else {
        CPU65X02_CLEAR_BRK(cpu->regs.status);
    }
}

void cpu6502_op_rti(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.status = cpu65x02_stack_pop(cpu);
    CPU65X02_SET_UNUSED(cpu->regs.status);
    cpu65x02_stack_pop_pc(cpu);
}

void cpu6502_op_rts(struct cpu65x02* cpu, uint16_t address) {
    cpu65x02_stack_pop_pc(cpu);
    cpu->regs.pc++;
}

void cpu6502_op_sbc(struct cpu65x02* cpu, uint16_t address) {
    uint8_t m = cpu->l(address, cpu->user_data);
    uint16_t tmp = cpu->regs.a - m - !CPU65X02_GET_CARRY(cpu->regs.status);

    cpu6502_sbc_decimal_adjust(cpu, m, &tmp);

    // must be done before original value in a is overwritten
    cpu65x02_calc_und(cpu, cpu->regs.a, m, tmp);
    cpu->regs.a = (uint8_t)(tmp & 0xff);
    cpu65x02_calc_sign_acc(cpu);
    cpu65x02_calc_zero_acc(cpu);
    cpu65x02_calc_borrow(cpu, tmp);
}

void cpu6502_op_sec(struct cpu65x02* cpu, uint16_t address) {
    CPU65X02_SET_CARRY(cpu->regs.status);
}

void cpu6502_op_sed(struct cpu65x02* cpu, uint16_t address) {
    CPU65X02_SET_DEC(cpu->regs.status);
}

void cpu6502_op_sei(struct cpu65x02* cpu, uint16_t address) {
    CPU65X02_SET_INT(cpu->regs.status);
}

void cpu6502_op_sta(struct cpu65x02* cpu, uint16_t address) {
    cpu->s(address, cpu->regs.a, cpu->user_data);
}

void cpu6502_op_stx(struct cpu65x02* cpu, uint16_t address) {
    cpu->s(address, cpu->regs.x, cpu->user_data);
}

void cpu6502_op_sty(struct cpu65x02* cpu, uint16_t address) {
    cpu->s(address, cpu->regs.y, cpu->user_data);
}

void cpu6502_op_tax(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.x = cpu->regs.a;
    cpu65x02_calc_sign(cpu, cpu->regs.x);
    cpu65x02_calc_zero(cpu, cpu->regs.x);
}

void cpu6502_op_tay(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.y = cpu->regs.a;
    cpu65x02_calc_sign(cpu, cpu->regs.y);
    cpu65x02_calc_zero(cpu, cpu->regs.y);
}

void cpu6502_op_tya(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.a = cpu->regs.y;
    cpu65x02_calc_sign(cpu, cpu->regs.y);
    cpu65x02_calc_zero(cpu, cpu->regs.y);
}

void cpu6502_op_tsx(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.x = cpu->regs.s;
    cpu65x02_calc_sign(cpu, cpu->regs.x);
    cpu65x02_calc_zero(cpu, cpu->regs.x);
}

void cpu6502_op_txa(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.a = cpu->regs.x;
    cpu65x02_calc_sign_acc(cpu);
    cpu65x02_calc_zero_acc(cpu);
}

void cpu6502_op_txs(struct cpu65x02* cpu, uint16_t address) {
    cpu->regs.s = cpu->regs.x;
}

void cpu6502_op_illegal(struct cpu65x02* cpu, uint16_t address) {
    (void)address;
    cpu65x02_dump_core(cpu);
    cpu65x02_fatal(cpu);
}

/* ========================================================================== */

void cpu6502_reset(struct cpu65x02 *cpu) {
	// clear the regs
	memset(&cpu->regs, 0x00, sizeof(struct cpu65x02_regs));

	CPU65X02_BIT_SET(cpu->regs.status, CPU65X02_STATUS_BIT_UNUSED);

	// set interrupt disabled flag
	CPU65X02_SET_INT(cpu->regs.status);

	// initialize the pc
    cpu65x02_load_vector(cpu, CPU65X02_RESET_VECTOR);

    // initialize stack pointer
	cpu->regs.s = 0xff;

    // re-initialize opcode pointer and cycles counters
    cpu->opcode = 0;
    cpu->cycles = 0;
    cpu->is_stopped = 0;
    cpu->is_waiting = 0;

    // initialization takes seven cycles
    cpu65x02_tick_n(cpu, 7);
}

void cpu6502_init(struct cpu65x02* cpu,
        cpu65x02_load_byte_t l,
        cpu65x02_store_byte_t s,
        void* user_data) {
    cpu->s = s;
    cpu->l = l;
    cpu->user_data = user_data;
    cpu->op_handlers = cpu6502_op_handlers;
    cpu->opcode = 0;
    cpu->cycles = 0;
    cpu->is_stopped = 0;
    cpu->is_waiting = 0;

	// clear the registers
	memset(&cpu->regs, 0x00, sizeof(cpu->regs));
}

/* ========================================================================== */
