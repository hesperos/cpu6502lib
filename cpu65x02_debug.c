#include "include/cpu65x02/cpu65x02_debug.h"
#include "include/cpu65x02/cpu65x02_constants.h"

#include <stdio.h>
#include <stdlib.h>

static const char* cpu65x02_opcode_mnemonics[CPU65X02_MAX_OPCODES] = {
    "brk", "ora", "ill", "ill", "tsb", "ora", "asl", "rmb0", // 0x00
    "php", "ora", "asl", "ill", "tsb", "ora", "asl", "bbr0", // 0x08
    "bpl", "ora", "ora", "ill", "trb", "ora", "asl", "rmb1", // 0x10
    "clc", "ora", "inca","ill", "trb", "ora", "asl", "bbr1", // 0x18
    "jsr", "and", "ill", "ill", "bit", "and", "rol", "rmb2", // 0x20
    "plp", "and", "rol", "ill", "bit", "and", "rol", "bbr2", // 0x28
    "bmi", "and", "and", "ill", "bit", "and", "rol", "rmb3", // 0x30
    "sec", "and", "deca","ill", "bit", "and", "rol", "bbr3", // 0x38
    "rti", "eor", "ill", "ill", "ill", "eor", "lsr", "rmb4", // 0x40
    "pha", "eor", "lsr", "ill", "jmp", "eor", "lsr", "bbr4", // 0x48
    "bvc", "eor", "eor", "ill", "ill", "eor", "lsr", "rmb5", // 0x50
    "cli", "eor", "phy", "ill", "ill", "eor", "lsr", "bbr5", // 0x58
    "rts", "adc", "ill", "ill", "stz", "adc", "ror", "rmb6", // 0x60
    "pla", "adc", "ror", "ill", "jmp", "adc", "ror", "bbr6", // 0x68
    "bvs", "adc", "adc", "ill", "stz", "adc", "ror", "rmb7", // 0x70
    "sei", "adc", "ply", "ill", "jmp", "adc", "ror", "bbr7", // 0x78
    "bra", "sta", "ill", "ill", "sty", "sta", "stx", "smb0", // 0x80
    "dey", "bit", "txa", "ill", "sty", "sta", "stx", "bbs0", // 0x88
    "bcc", "sta", "sta", "ill", "sty", "sta", "stx", "smb1", // 0x90
    "tya", "sta", "txs", "ill", "stz", "sta", "stz", "bbs1", // 0x98
    "ldy", "lda", "ldx", "ill", "ldy", "lda", "ldx", "smb2", // 0xa0
    "tay", "lda", "tax", "ill", "ldy", "lda", "ldx", "bbs2", // 0xa8
    "bcs", "lda", "lda", "ill", "ldy", "lda", "ldx", "smb3", // 0xb0
    "clv", "lda", "tsx", "ill", "ldy", "lda", "ldx", "bbs3", // 0xb8
    "cpy", "cmp", "ill", "ill", "cpy", "cmp", "dec", "smb4", // 0xc0
    "iny", "cmp", "dex", "wai", "cpy", "cmp", "dec", "bbs4", // 0xc8
    "bne", "cmp", "cmp", "ill", "ill", "cmp", "dec", "smb5", // 0xd0
    "cld", "cmp", "phx", "stp", "ill", "cmp", "dec", "bbs5", // 0xd8
    "cpx", "sbc", "ill", "ill", "cpx", "sbc", "inc", "smb6", // 0xe0
    "inx", "sbc", "nop", "ill", "cpx", "sbc", "inc", "bbs6", // 0xe8
    "beq", "sbc", "sbc", "ill", "ill", "sbc", "inc", "smb7", // 0xf0
    "sed", "sbc", "plx", "ill", "ill", "sbc", "inc", "bbs7", // 0xf8
};

void cpu65x02_dump_core(struct cpu65x02* cpu) {
    fprintf(stderr, "==================\n");
    fprintf(stderr, "a: 0x%02x\n", cpu->regs.a);
    fprintf(stderr, "x: 0x%02x, y: 0x%02x\n", cpu->regs.x, cpu->regs.y);
    fprintf(stderr, "s: 0x%02x\n", cpu->regs.s);
    fprintf(stderr, "status: 0x%02x\n", cpu->regs.status);
    fprintf(stderr, "pc: 0x%04x\n", cpu->regs.pc);
    fprintf(stderr, "cycles: %u\n", cpu->cycles);
    fprintf(stderr, "==================\n");
}

void cpu65x02_disasm_opcode(struct cpu65x02* cpu,
        uint16_t address) {
    fprintf(stderr, "%02x(%4s)@%04x, addr: %04x, "
            "r[%02x %02x %02x %02x %02x], ic: %u, c: %u\n",
            cpu->opcode, cpu65x02_opcode_mnemonics[cpu->opcode],
            cpu->regs.pc, address,
            cpu->regs.a, cpu->regs.x, cpu->regs.y,
            cpu->regs.s, cpu->regs.status,
            cpu->op_handlers[cpu->opcode].base_cycles,
            cpu->cycles);
}

void cpu65x02_fatal(struct cpu65x02* cpu) {
    fprintf(stderr, "Fatal error. Stopping execution");
    exit(1);
}

